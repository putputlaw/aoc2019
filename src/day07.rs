use crate::intcode::IntCodeMachine;
use itertools::Itertools;

pub fn main() {
    part1();
    part2();
}

fn get_input() -> IntCodeMachine {
    IntCodeMachine::from_string(include_str!("../inputs/07.txt"))
}

fn part1() {
    let program = get_input();
    println!(
        "Part 1: {}",
        (0..5)
            .permutations(5)
            .map(|perm| {
                let mut input = 0;
                for phase in perm {
                    let mut program = program
                        .clone()
                        .with_input(vec![phase])
                        .with_fallback_input(input);
                    if let Some(o) = program.next_output() {
                        input = o
                    }
                }
                input
            })
            .max()
            .expect("the unexpected")
    )
}

fn part2() {
    let program = get_input();
    println!(
        "Part 2: {}",
        (5..10)
            .permutations(5)
            .map(|perm| {
                let mut input = 0;
                let mut last_thruster_signal = 0;
                let amplifiers = &mut perm
                    .into_iter()
                    .map(|phase| program.clone().with_input(vec![phase]))
                    .collect::<Vec<_>>()[..];
                'a: loop {
                    for amplifier in amplifiers.iter_mut() {
                        amplifier.inputs.push(input);
                        let output = amplifier.next_output();
                        match output {
                            Some(o) => input = o,
                            None => break 'a,
                        }
                    }
                    last_thruster_signal = input;
                }
                last_thruster_signal
            })
            .max()
            .expect("the unexpected")
    )
}
