use crate::intcode::*;
use std::collections::HashMap;

const PUZZLE_INPUT: &str = include_str!("../inputs/13.txt");

pub fn main() {
    part1();
    part2();
}

fn get_input() -> IntCodeMachine {
    IntCodeMachine::from_string(PUZZLE_INPUT)
}

fn part1() {
    let mut program = get_input();
    let mut map = HashMap::new();
    while let (Some(x), Some(y), Some(tile)) = (
        program.next_output(),
        program.next_output(),
        program.next_output(),
    ) {
        map.insert((x, y), tile);
    }

    println!("Part 1: {}", map.iter().filter(|(_, v)| **v == 2).count())
}

fn part2() {
    let mut program = get_input();
    program.inputs.push_vec(vec![0,1]);
    program.mem.put(0, 2);
    let mut ball_x = 0;
    let mut paddle_x = 0;
    let mut score = 0;
    while let (Some(x), Some(y), Some(tile)) = (
        program.next_output(),
        program.next_output(),
        program.next_output(),
    ) {
        match (x, y, tile) {
            (-1, 0, s) => score = s,
            (x, _, 3) => paddle_x = x,
            (x, _, 4) => ball_x = x,
            _ => {}
        }
        program.inputs.fallback = Some((ball_x - paddle_x).signum());
    }
    println!("Part 2: {}", score)
}
