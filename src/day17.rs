use crate::intcode::*;
use itertools::Itertools;

const PUZZLE_INPUT: &str = include_str!("../inputs/17.txt");

pub fn main() {
    let program = parse_input(PUZZLE_INPUT);
    let map = read_map(program.clone());
    part1(&map);
    part2(program, map);
}

fn parse_input(input: &str) -> IntCodeMachine {
    IntCodeMachine::from_string(input)
}

fn read_map(mut program: IntCodeMachine) -> path_finder::Map {
    let map_string: String = program
        .run()
        .into_iter()
        .map(|o| (o as u8) as char)
        .collect();
    path_finder::Map::from_string(&map_string)
}

fn part1(map: &path_finder::Map) {
    println!(
        "Part 1: {}",
        map.contents
            .iter()
            .filter(|(_, v)| **v == path_finder::Tile::Intersection)
            .map(|((row, col), _)| row * col)
            .sum::<i64>()
    );
}

fn part2(program: IntCodeMachine, map: path_finder::Map) {
    let actions = map.into_actions();
    for (dict, code) in path_finder::compress(&actions) {
        //     // HACK: manually got the solution (some sort of greedy approach)
        //     let commands = "A,B,A,C,A,B,A,C,B,C
        // R,4,L,12,L,8,R,4
        // L,8,R,10,R,10,R,6
        // R,4,R,10,L,12
        // n
        // ";
        let commands = format!(
            "{}\n{}\n{}\n{}\nn\n",
            code.iter().map(|ch| ch.to_string()).join(","),
            path_finder::print_moves(&dict[&'A']),
            path_finder::print_moves(&dict[&'B']),
            path_finder::print_moves(&dict[&'C']),
        );
        let mut program = program.clone();
        program.mem.put(0, 2);
        program
            .inputs
            .push_vec(commands.chars().map(|c| c as i64).collect());
        if let Some(score) = program.run().last() {
            println!("Part 2: {}", score);
            return;
        }
    }
}

pub mod path_finder {
    use crate::bot::CardDir;
    use itertools::Itertools;
    use std::collections::{HashMap, HashSet};

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct Map {
        pub num_cols: usize,
        pub num_rows: usize,
        pub contents: HashMap<(i64, i64), Tile>,
        pub bot_pos: (i64, i64),
        pub bot_dir: CardDir,
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum Tile {
        Path,
        Intersection,
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum Move {
        Left,
        Right,
        Forward(usize),
    }

    impl Map {
        pub fn from_string(input: &str) -> Self {
            let num_rows = input.lines().count();
            let num_cols = input.lines().next().unwrap().len();
            let mut contents = HashMap::new();
            let mut row = 0;
            let mut col = 0;
            let mut bot_dir = None;
            let mut bot_pos = None;
            for ch in input.chars() {
                match ch {
                    '\n' => {
                        col = 0;
                        row += 1;
                    }
                    '.' => {
                        col += 1;
                    }
                    '#' => {
                        contents.insert((row, col), Tile::Path);
                        col += 1;
                    }
                    '^' | '<' | '>' | 'v' => {
                        contents.insert((row, col), Tile::Path);
                        bot_pos = Some((row, col));
                        match ch {
                            '^' => bot_dir = Some(CardDir::North),
                            '>' => bot_dir = Some(CardDir::East),
                            'v' => bot_dir = Some(CardDir::South),
                            '<' => bot_dir = Some(CardDir::West),
                            _ => {}
                        };
                        col += 1;
                    }
                    _ => panic!("invalid Map format"),
                }
            }

            // mark intersections
            let num_inner_rows = (num_rows - 1) as i64;
            let num_inner_cols = (num_cols - 1) as i64;
            let intersections: Vec<_> = iproduct!(1..num_inner_rows, 1..num_inner_cols)
                .filter(|&(row, col)| {
                    vec![
                        (row, col),
                        (row - 1, col),
                        (row + 1, col),
                        (row, col - 1),
                        (row, col + 1),
                    ]
                    .into_iter()
                    .all(|pt| contents.get(&pt).cloned() == Some(Tile::Path))
                })
                .collect();
            for pt in intersections {
                contents.insert(pt, Tile::Intersection);
            }
            Map {
                num_rows,
                num_cols,
                contents,
                bot_pos: bot_pos.expect("no bot found"),
                bot_dir: bot_dir.expect("no bot found"),
            }
        }

        /// several assumptions:
        /// - first action is a turn
        /// - turning and moving forward alternate
        /// - at intersection go straight
        /// - etc.
        pub fn into_actions(mut self) -> Vec<Move> {
            assert_eq!(self.peek_ahead(), None);
            let mut actions = vec![];
            let mut places: HashSet<(i64, i64)> = self.contents.keys().cloned().collect();
            places.remove(&self.bot_pos);
            let mut forward_steps = 0;
            while !places.is_empty() {
                if self.peek_ahead().is_some() {
                    self.go_ahead();
                    forward_steps += 1;
                } else if self.peek_left().is_some() {
                    if forward_steps > 0 {
                        actions.push(Move::Forward(forward_steps));
                    }
                    actions.push(Move::Left);
                    self.turn_left();
                    self.go_ahead();
                    forward_steps = 1;
                } else if self.peek_right().is_some() {
                    if forward_steps > 0 {
                        actions.push(Move::Forward(forward_steps));
                    }
                    actions.push(Move::Right);
                    self.turn_right();
                    self.go_ahead();
                    forward_steps = 1;
                } else {
                    panic!("path finding ended prematurely")
                }
                places.remove(&self.bot_pos);
            }
            if forward_steps > 0 {
                actions.push(Move::Forward(forward_steps));
            }
            actions
        }

        pub fn peek_ahead(&self) -> Option<Tile> {
            let pt = self.bot_dir.apply(self.bot_pos);
            self.contents.get(&pt).cloned()
        }

        pub fn peek_left(&self) -> Option<Tile> {
            let pt = self.bot_dir.turn_left().apply(self.bot_pos);
            self.contents.get(&pt).cloned()
        }

        pub fn peek_right(&self) -> Option<Tile> {
            let pt = self.bot_dir.turn_right().apply(self.bot_pos);
            self.contents.get(&pt).cloned()
        }

        pub fn go_ahead(&mut self) {
            self.bot_pos = self.bot_dir.apply(self.bot_pos);
        }

        pub fn turn_left(&mut self) {
            self.bot_dir = self.bot_dir.turn_left();
        }

        pub fn turn_right(&mut self) {
            self.bot_dir = self.bot_dir.turn_right();
        }
    }

    type CompressedMoves = (HashMap<char, Vec<Move>>, Vec<char>);

    pub fn compress(moves: &[Move]) -> Vec<CompressedMoves> {
        let mut out = vec![];
        for a_len in (2..10).step_by(2) {
            let a = &moves[0..a_len];
            let mut b_start = a_len;
            for b_len in (2..10).step_by(2) {
                while *a == moves[b_start..b_start + a_len] {
                    b_start += a_len;
                }
                let b = &moves[b_start..b_start + b_len];
                let mut c_start = b_start + b_len;
                for c_len in (2..10).step_by(2) {
                    loop {
                        let next_is_a =
                            c_start + a_len <= moves.len() && *a == moves[c_start..c_start + a_len];
                        let next_is_b =
                            c_start + b_len <= moves.len() && *b == moves[c_start..c_start + b_len];
                        if next_is_a {
                            c_start += a_len
                        } else if next_is_b {
                            c_start += b_len
                        } else {
                            break;
                        }
                    }
                    if c_start + c_len > moves.len() {
                        break;
                    }
                    let c = &moves[c_start..c_start + c_len];
                    let dict = vec![('A', a), ('B', b), ('C', c)].into_iter().collect();
                    if let Some(ans) = compress_with_dict(moves, &dict) {
                        let owned_dict: HashMap<char, Vec<Move>> =
                            dict.into_iter().map(|(k, v)| (k, v.to_vec())).collect();
                        assert_eq!(
                            ans.iter()
                                .flat_map(|ch| owned_dict.get(ch).unwrap())
                                .cloned()
                                .collect::<Vec<_>>(),
                            moves.to_vec()
                        );
                        out.push((owned_dict, ans))
                    }
                }
            }
        }
        out
    }

    #[allow(clippy::implicit_hasher)]
    pub fn compress_with_dict(moves: &[Move], dict: &HashMap<char, &[Move]>) -> Option<Vec<char>> {
        let mut offset = 0;
        let mut out = vec![];
        loop {
            let mut ok = false;
            for (fun, routine) in dict {
                if offset + routine.len() <= moves.len()
                    && **routine == moves[offset..offset + routine.len()]
                {
                    offset += routine.len();
                    out.push(*fun);
                    ok = true;
                    break;
                }
            }
            if !ok {
                return None;
            } else if offset == moves.len() {
                return Some(out);
            }
        }
    }

    pub fn print_moves(moves: &[Move]) -> String {
        moves
            .iter()
            .map(|m| match m {
                Move::Forward(num) => num.to_string(),
                Move::Left => "L".to_string(),
                Move::Right => "R".to_string(),
            })
            .join(",")
    }
}
