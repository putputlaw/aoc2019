use maze::*;

const PUZZLE_INPUT: &str = include_str!("../inputs/20.txt");

pub fn main() {
    let tile_array = TileArray::from_string(PUZZLE_INPUT);
    let maze = Maze::from_tile_array(&tile_array);

    for (label, dist) in maze.as_looping().iter() {
        if label == Label::finish_label() {
            println!("Part 1: {}", dist);
            break;
        }
    }

    for ((label, level), dist) in maze.as_levelling().iter() {
        if label == Label::finish_label() && level == 0 {
            println!("Part 2: {}", dist);
            break;
        }
    }
}

pub mod maze {
    use crate::geometry::point::Point;
    use crate::geometry::rectangle::Rectangle;
    use crate::geometry::tiles::Tiles;
    use crate::shortest_paths::dijkstra::{DijkstraIter, WGraph};
    use std::collections::{HashMap, HashSet};

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum Tile {
        L(char), // partial label
        P,       // passage
    }

    pub type Distance = usize;
    pub type Level = usize;

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct Label {
        a: char,
        b: char,
        pos: LabelPos,
    }

    impl Label {
        pub fn new(a: char, b: char, pos: LabelPos) -> Self {
            Label { a, b, pos }
        }

        pub fn start_label() -> Self {
            Label::new('A', 'A', LabelPos::Outer)
        }

        pub fn finish_label() -> Self {
            Label::new('Z', 'Z', LabelPos::Outer)
        }

        pub fn flip(&self) -> Self {
            Label::new(self.a, self.b, self.pos.flip())
        }
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum LabelPos {
        Inner,
        Outer,
    }

    impl LabelPos {
        pub fn flip(self) -> Self {
            match self {
                LabelPos::Inner => LabelPos::Outer,
                LabelPos::Outer => LabelPos::Inner,
            }
        }
    }

    // isolated level -- no portals!
    #[derive(Debug, Clone)]
    pub struct TileArray {
        tiles: Tiles<Tile>,
        interior: Rectangle,
    }

    impl TileArray {
        pub fn from_string(input: &str) -> Self {
            let mut portal_area = Rectangle::Empty;

            let tiles = Tiles::from_string(input, |_, ch| match ch {
                '.' => Some(Tile::P),
                ch if ('A'..='Z').contains(&ch) => Some(Tile::L(ch)),
                ' ' | '#' => None,
                _ => panic!("invalid char {:?}", ch),
            });

            for (pt, tile) in tiles.to_vec() {
                if tile == Tile::P {
                    portal_area = portal_area.grow(pt)
                }
            }

            TileArray {
                tiles,
                interior: portal_area.interior(),
            }
        }

        pub fn label(&self, (x, y): Point) -> Option<Label> {
            for (pt0, pt1) in &[
                ((x, y - 1), (x, y - 2)),
                ((x, y + 2), (x, y + 1)),
                ((x - 2, y), (x - 1, y)),
                ((x + 1, y), (x + 2, y)),
            ] {
                if let [(_, Tile::L(a)), (_, Tile::L(b))] = &self.tiles.get_many(&[*pt0, *pt1])[..]
                {
                    return Some(Label {
                        a: *a,
                        b: *b,
                        pos: {
                            if self.interior.contains((x, y)) {
                                LabelPos::Inner
                            } else {
                                LabelPos::Outer
                            }
                        },
                    });
                }
            }
            None
        }

        pub fn locate_label(&self, target: Label) -> Option<Point> {
            self.tiles.keys().into_iter().find(|pt| {
                self.label(*pt)
                    .map(|label| label == target)
                    .unwrap_or(false)
            })
        }

        pub fn iter<'a>(&'a self, label: Label) -> impl Iterator<Item = (Point, Distance)> + 'a {
            self.locate_label(label)
                .map(|pt| DijkstraIter::new(self, pt))
                .into_iter()
                .flatten()
        }
    }

    impl WGraph for TileArray {
        type Node = Point;
        type Distance = Distance;

        fn zero() -> Self::Distance {
            0
        }

        fn neighbours(&self, pt: &Self::Node) -> Vec<(Self::Node, Self::Distance)> {
            self.tiles
                .get_many(&[
                    (pt.0, pt.1 - 1),
                    (pt.0, pt.1 + 1),
                    (pt.0 - 1, pt.1),
                    (pt.0 + 1, pt.1),
                ])
                .into_iter()
                .flat_map(|(pt, tile)| match tile {
                    Tile::P => Some((pt, 1)),
                    Tile::L(_) => None,
                })
                .collect()
        }
    }

    #[derive(Debug, Clone)]
    pub struct Maze {
        /// neighbours reachable by walking + distance
        pub rneighbours: HashMap<Label, HashSet<(Label, Distance)>>,
        /// label for given point
        labels: HashMap<Point, Label>,
        /// point for given label
        portals: HashMap<Label, Point>,
    }

    impl Maze {
        pub fn from_string(input: &str) -> Self {
            Maze::from_tile_array(&TileArray::from_string(input))
        }

        pub fn from_tile_array(input: &TileArray) -> Self {
            let mut portals = HashMap::new();
            let mut labels = HashMap::new();
            for (label, portal) in input.tiles.to_vec().into_iter().flat_map(|(pt, tile)| {
                if let (Tile::P, Some(label)) = (tile, input.label(pt)) {
                    Some((label, pt))
                } else {
                    None
                }
            }) {
                portals.insert(label, portal);
                labels.insert(portal, label);
            }

            let mut rneighbours = HashMap::new();
            for (label, _) in portals.iter() {
                // NOTE: most of the hard work (BFS searches) is done here
                for (pt, dist) in input.iter(*label) {
                    if dist > 0 {
                        if let Some(nd_label) = input.label(pt) {
                            rneighbours
                                .entry(*label)
                                .or_insert_with(HashSet::new)
                                .insert((nd_label, dist));
                        }
                    }
                }
            }
            Maze {
                rneighbours,
                labels,
                portals,
            }
        }

        pub fn as_looping(&self) -> LoopingMaze {
            LoopingMaze(self)
        }

        pub fn as_levelling(&self) -> LevellingMaze {
            LevellingMaze(self)
        }
    }

    /// for part 1
    pub struct LoopingMaze<'a>(&'a Maze);

    impl<'a> WGraph for LoopingMaze<'a> {
        type Node = Label;
        type Distance = Distance;

        fn zero() -> Self::Distance {
            0
        }

        fn neighbours(&self, node: &Self::Node) -> Vec<(Self::Node, Self::Distance)> {
            self.0
                .rneighbours
                .get(&node)
                .into_iter()
                .flatten()
                .cloned()
                .chain(
                    {
                        if *node != Label::start_label() && *node != Label::finish_label() {
                            Some((node.flip(), 1))
                        } else {
                            None
                        }
                    }
                    .into_iter(),
                )
                .collect()
        }
    }

    impl<'a> LoopingMaze<'a> {
        pub fn iter(&self) -> DijkstraIter<Self> {
            DijkstraIter::new(self, Label::start_label())
        }
    }

    /// for part 2
    pub struct LevellingMaze<'a>(&'a Maze);

    impl<'a> LevellingMaze<'a> {
        pub fn iter(&self) -> DijkstraIter<Self> {
            DijkstraIter::new(self, (Label::start_label(), 0))
        }
    }

    impl<'a> WGraph for LevellingMaze<'a> {
        type Node = (Label, Level);
        type Distance = Distance;

        fn zero() -> Self::Distance {
            0
        }

        fn neighbours(&self, (label, level): &Self::Node) -> Vec<(Self::Node, Self::Distance)> {
            self.0
                .rneighbours
                .get(label)
                .into_iter()
                .flatten()
                .map(|(nd_label, edge_len)| ((*nd_label, *level), *edge_len))
                .chain(
                    {
                        if *label != Label::start_label() && *label != Label::finish_label() {
                            match label.pos {
                                LabelPos::Outer if *level > 0 => {
                                    Some(((label.flip(), *level - 1), 1))
                                }
                                LabelPos::Inner => Some(((label.flip(), *level + 1), 1)),
                                _ => None,
                            }
                        } else {
                            None
                        }
                    }
                    .into_iter(),
                )
                .collect()
        }
    }
}
