use geometry::*;
use itertools::Itertools;
use std::collections::{HashMap, HashSet};

const PUZZLE_INPUT: &str = include_str!("../inputs/10.txt");

pub fn main() {
    let observer = part1();
    part2(observer);
}

fn parse_input(input: &str) -> HashSet<Point> {
    input
        .trim()
        .lines()
        .enumerate()
        .flat_map(|(y, line)| {
            line.chars()
                .enumerate()
                .map(move |(x, ch)| (x as i32, y as i32, ch))
        })
        .flat_map(|(x, y, ch)| {
            if ch == '#' {
                Some(Point::new(x, y))
            } else {
                None
            }
        })
        .collect()
}

fn part1() -> Point {
    let asteroids = parse_input(PUZZLE_INPUT);
    let mut angles: HashMap<Point, HashSet<Angle>> = HashMap::new();
    for (&observer, &observed) in iproduct!(&asteroids, &asteroids) {
        if observer != observed {
            angles
                .entry(observer)
                .or_insert_with(HashSet::new)
                .insert(observer.angle(observed));
        }
    }

    let (observer, num_observed) = angles
        .into_iter()
        .map(|(angle, ray)| (angle, ray.len()))
        .max_by_key(|r| r.1)
        .expect("at most asteroids on the map");

    println!("Part 1: {}", num_observed);
    observer
}

fn part2(observer: Point) {
    let mut asteroids = parse_input(PUZZLE_INPUT);
    // dont shoot ourselves!
    asteroids.remove(&observer);
    // 1. inside transpose(..): get a list of asteroids sorted by angle
    //    asteroids of the same angle will be grouped up
    // 2. transpose then transforms the list of rows into a list of columns
    //    (similar to matrix transposition)
    //    for instance, the first column is the list of the closest asteroids in each group
    //    the second column then is the list of the second closest asteroids in each group
    // 3. technical note: by lists I mean iterators
    let asteroids200 = transpose(
        asteroids
            .into_iter()
            .map(|observed| {
                (
                    observer.angle(observed),
                    observer.l1_norm(observed),
                    observed,
                )
            })
            // order asteroids by angle and group same angles
            .sorted_by_key(|o| o.0)
            .group_by(|o| o.0)
            .into_iter()
            // sort each asteroid on a ray by distance observer
            // and drop angle/distance information
            .map(|(_, group)| group.sorted_by_key(|o| o.1).map(|o| o.2)),
    )
    .flatten()
    .nth(199);

    if let Some(point) = asteroids200 {
        println!("Part 2: {:?}", point.x * 100 + point.y)
    }
}

/// Example: [[a,b,c],[d,e],[f]] -> [[a,d,f],[b,e],[c]]
fn transpose<T, I, J>(arr: I) -> impl Iterator<Item = Vec<T>>
where
    T: Clone,
    I: Iterator<Item = J>,
    J: Iterator<Item = T>,
{
    let mut rows = arr.collect::<Vec<_>>();
    std::iter::from_fn(move || {
        let (column, rest): (Vec<_>, _) = std::mem::replace(&mut rows, vec![])
            .into_iter()
            .flat_map(|mut row| row.next().map(|head| (head, row)))
            .unzip();
        rows = rest;
        if column.is_empty() {
            None
        } else {
            Some(column)
        }
    })
}

pub mod geometry {
    use std::cmp::Ordering;

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct Point {
        pub x: i32,
        pub y: i32,
    }

    impl Point {
        pub fn new(x: i32, y: i32) -> Point {
            Point { x, y }
        }

        pub fn angle(self, other: Self) -> Angle {
            let dx = other.x - self.x;
            let dy = other.y - self.y;
            Angle::new(dx, dy).unwrap()
        }

        pub fn l1_norm(self, other: Self) -> u32 {
            ((self.x - other.x).abs() + (self.y - other.y).abs()) as u32
        }
    }

    #[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
    /// An angle is represented by a vector (dx, dy) pointing in the direction of the angle.
    /// In order for this representation to be unique, `dx` and `dy` must be coprime (have no common divisor).
    /// Compared to the more conventional arc length representation, with angles being
    /// represented by real numbers in the half-open interval [0,2*pi), we obtain *higher precision*
    /// by avoiding floating point numbers.
    ///
    /// Still, it is helpful to view the angles via the more conventional arc length representation.
    /// In terms of arc-length, the the representation `(dx,dy)=(0,1)` is assigned the angle 0,
    /// and angles grow clockwise. For instance `(dx,dy)=(1,0)` has angle pi/2.
    ///
    /// How can we implement `Ord` for `Angle`?
    /// A first, very crude, approximation is by computing the quadrant which contains (dx, dy).
    /// We assign the top-right quadrant the value 0, the bottom-right quadrant the value 1,
    /// the bottom-left quadrant the value 2, and the top-left quadrant the value 3.
    /// By doing so, we can compare angles by comparing their quadrants.
    /// This works, unless the quadrants are the same.
    ///
    /// Let's say two angles `a` and `b` lie in the same quadrant.
    /// Then we can imagine a line L through `a` and `(0,0)`. On which side of this line `b` lies
    /// determines whether `b` is greater or smaller than `a`.
    /// Mathematically this question can be answered by inspecting a certain dot product ...
    /// Figuring this out isn't hard though and I encourage the dear reader to
    /// do this exercise on pen and paper ;)
    pub struct Angle {
        dx: i32,
        dy: i32,
    }

    impl Angle {
        pub fn new(dx: i32, dy: i32) -> Option<Angle> {
            let gcd = num::integer::gcd(dx, dy);
            assert!(gcd > 0);
            if gcd == 0 {
                None
            } else {
                Some(Angle {
                    dx: dx / gcd,
                    dy: dy / gcd,
                })
            }
        }

        pub fn quadrant(self) -> usize {
            // NOTE that the image coordinate system is slightly different from the one used in mathematics
            match (self.dx >= 0, self.dy <= 0) {
                (true, true) => 0,
                (true, false) => 1,
                (false, false) => 2,
                (false, true) => 3,
            }
        }
    }

    impl Ord for Angle {
        fn cmp(&self, other: &Self) -> Ordering {
            (self.quadrant(), -other.dy * self.dx + other.dx * self.dy).cmp(&(other.quadrant(), 0))
        }
    }

    impl PartialOrd for Angle {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            Some(self.cmp(&other))
        }
    }
}
