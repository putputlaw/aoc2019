use crate::intcode::*;

pub fn main() {
    part1();
    part2();
}

fn get_input() -> IntCodeMachine {
    IntCodeMachine::from_string(include_str!("../inputs/05.txt"))
}

fn part1() {
    let mut program = get_input().with_fallback_input(1);
    if let Some(output) = program.run().last() {
        println!("Part 1: {:?}", output)
    }
}

fn part2() {
    let mut program = get_input().with_fallback_input(5);
    if let Some(output) = program.run().last() {
        println!("Part 2: {:?}", output)
    }
}
