use core::ops::Range;
use itertools::Itertools;
use kmp::kmp_match;

/// Split the slices of an array `data` by a `needle`.
///
/// Assumption on the `needle`: No non-empty prefix of needle is a suffix of needle,
///   otherwise some needles get dropped, until there is no overlap (roughly speaking)
pub fn split_ranges<T>(data: &[T], ranges: &[Range<usize>], needle: &[T]) -> Vec<Range<usize>>
where
    T: Clone + PartialEq,
{
    if ranges.is_empty() {
        vec![]
    } else {
        ranges
            .iter()
            .flat_map(|r| {
                Some(0)
                    .into_iter()
                    .chain(
                        kmp_match(needle, &data[r.clone()])
                            .into_iter()
                            .flat_map(|m| vec![m, m + needle.len()]),
                    )
                    .chain(Some(r.end - r.start))
                    .chunks(2)
                    .into_iter()
                    .flat_map(|mut c| {
                        let lb = c.next().unwrap();
                        let ub = c.next().unwrap();
                        if lb == ub {
                            None
                        } else {
                            Some(lb..ub)
                        }
                    })
                    .collect::<Vec<_>>()
            })
            .collect()
    }
}
