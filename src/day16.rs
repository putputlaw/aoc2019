use std::io::{stdout, Write};
use std::iter::repeat;

const PUZZLE_INPUT: &str = include_str!("../inputs/16.txt");

pub fn main() {
    let signal = parse_input(PUZZLE_INPUT);
    part1(&signal);
    part2(&signal);
}

fn parse_input(input: &str) -> Vec<i64> {
    input
        .chars()
        .flat_map(|ch| ch.to_digit(10))
        .map(|d| d as i64)
        .collect()
}

fn part1(signal: &[i64]) {
    let mut signal = signal.to_vec();
    for _ in 0..100 {
        signal = fft::convolve(&signal, 0);
    }
    println!("Part 1: {:?}", fft::to_decimal(&signal[0..8]));
}

fn part2(base_signal: &[i64]) {
    let offset = fft::to_decimal(&base_signal[0..7]) as usize;
    let mut signal: Vec<_> = repeat(base_signal.iter())
        .flatten()
        .cloned()
        .take(base_signal.len() * 10_000)
        // The convolution is a multiplication by a huge upper-triangular matrix.
        // This means that the i-th entry of the output does not depend on
        // any j-th entry, j < i, of the input vector.
        // As optimization we can therefore ignore the first `offset` values of the input.
        // idea from https://github.com/madmax28/aoc2019/blob/master/src/day16/mod.rs
        .skip(offset)
        .collect();
    for i in 0..100 {
        // NOTE: quadratic complexity is seriously bad here
        signal = fft::convolve(&signal, offset);
        print!("\r{} done", i);
        stdout().flush().unwrap();
    }
    println!("\rPart 2: {:?}", fft::to_decimal(&signal[0..8]));
}

pub mod fft {
    use rayon::prelude::*;

    // The key idea in https://github.com/jaspervdj/advent-of-code/blob/master/2019/16.hs
    // consists of precomputing the prefix sums, so we can compute in O(1)
    // the sum of consecutive digits.
    // This cuts the naive O(n^2) time complexity down to O(n log(n)).
    pub fn convolve(signal: &[i64], offset: usize) -> Vec<i64> {
        let prefix_sums: Vec<i64> = vec![0]
            .into_iter()
            .chain(signal.iter().scan(0, |acc, s| {
                *acc += s;
                Some(*acc)
            }))
            .collect();

        (1 + offset..signal.len() + 1 + offset)
            .into_par_iter()
            .map(|width| {
                last_digit(
                    (offset / width..=(offset + signal.len() + 1) / width).fold(
                        0,
                        |acc, window| {
                            let segment_sum = {
                                let ub = (((window + 1) * width).saturating_sub(1 + offset))
                                    .min(signal.len());
                                let lb = (window * width)
                                    .saturating_sub(1 + offset)
                                    .min(signal.len());
                                prefix_sums[ub] - prefix_sums[lb]
                            };
                            match window % 4 {
                                1 => acc + segment_sum,
                                3 => acc - segment_sum,
                                _ => acc,
                            }
                        },
                    ),
                )
            })
            .collect()
    }

    fn last_digit(num: i64) -> i64 {
        num.abs() % 10
    }

    pub fn to_decimal(digits: &[i64]) -> i64 {
        // assert digits between 0 and 9 inclusive
        digits.iter().fold(0, |acc, i| acc * 10 + i)
    }
}

#[cfg(test)]
mod test {
    use super::fft;

    #[test]
    fn test_1() {
        assert_eq!(
            fft::convolve(&vec![1, 2, 3, 4, 5, 6, 7, 8], 0),
            vec![4, 8, 2, 2, 6, 1, 5, 8]
        );
    }

    #[test]
    fn test_2() {
        assert_eq!(
            fft::convolve(&vec![2, 3, 4, 5, 6, 7, 8], 1),
            vec![8, 2, 2, 6, 1, 5, 8]
        );
    }
}
