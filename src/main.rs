use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(short = "d", long = "day")]
    day: usize,
}

fn main() {
    aoc2019::run(Opt::from_args().day)
}
