use geometry::{Intersection, LineSegment, Point2d};
use std::collections::HashMap;

pub fn main() {
    // part 1 is called by part 2 as dependency
    part2()
}

fn get_input() -> (Vec<LineSegment>, Vec<LineSegment>) {
    let deltass = include_str!("../inputs/03.txt")
        .lines()
        .map(|line| {
            line.trim()
                .split(',')
                .flat_map(|text| {
                    let (dir, num) = text.split_at(1);
                    let n = num.parse::<i64>().ok()?;
                    match dir {
                        "U" => Some(Point2d::new(0, n)),
                        "R" => Some(Point2d::new(n, 0)),
                        "D" => Some(Point2d::new(0, -n)),
                        "L" => Some(Point2d::new(-n, 0)),
                        _ => None,
                    }
                })
                .collect::<Vec<_>>()
        })
        .map(|deltas| segments(&deltas))
        .collect::<Vec<_>>();
    if let [segs0, segs1] = &deltass[..] {
        (segs0.clone(), segs1.clone())
    } else {
        panic!("invalid input")
    }
}

fn part1() -> Vec<Point2d> {
    let (segs0, segs1) = get_input();

    let mut intersections = iproduct!(segs0, segs1)
        .filter_map(|(s0, s1)| match s0.intersect(&s1) {
            Intersection::Point(pt) => Some(pt),
            _ => None,
        })
        .collect::<Vec<_>>();
    intersections.sort_by_key(|r| r.l1_dist(&Point2d::origin()));
    let best = intersections[1]; // ignore origin
    println!("Part 1: {}", best.l1_dist(&Point2d::origin()));
    intersections
}

fn segments(deltas: &[Point2d]) -> Vec<LineSegment> {
    deltas
        .iter()
        .scan(Point2d::origin(), |prev, delta| {
            let _prev = *prev;
            *prev = prev.add(delta);
            Some(LineSegment {
                from: _prev,
                to: *prev,
            })
        })
        .collect()
}

fn part2() {
    let intersections = part1();
    let (segs0, segs1) = get_input();

    let dist_maps = [segs0, segs1]
        .iter()
        .map(|segs| {
            let mut wire_dist = HashMap::<Point2d, u64>::new();
            let mut dist = 0;
            for seg in segs {
                for ipt in intersections.iter() {
                    if seg.contains(&ipt) && !wire_dist.contains_key(ipt) {
                        wire_dist.insert(
                            *ipt,
                            dist + LineSegment {
                                from: seg.from,
                                to: *ipt,
                            }
                            .length(),
                        );
                    }
                }
                dist += seg.length();
            }
            wire_dist
        })
        .collect::<Vec<_>>();

    let map0 = dist_maps[0].clone();
    let map1 = dist_maps[1].clone();

    let mut with_wire_dist = intersections
        .iter()
        .map(|ipt| (ipt, map0[ipt] + map1[ipt]))
        .collect::<Vec<_>>();

    with_wire_dist.sort_by_key(|x| x.1);
    let best = with_wire_dist[1];

    println!("Part 2: {}", best.1);
}

pub mod geometry {
    use std::cmp::{max, min, Ordering};

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub struct Point2d {
        pub x: i64,
        pub y: i64,
    }

    impl Point2d {
        pub fn new(x: i64, y: i64) -> Self {
            Point2d { x, y }
        }

        pub fn origin() -> Self {
            Self::new(0, 0)
        }

        pub fn add(&self, other: &Self) -> Self {
            Point2d {
                x: self.x + other.x,
                y: self.y + other.y,
            }
        }

        pub fn sub(&self, other: &Self) -> Self {
            Point2d {
                x: self.x - other.x,
                y: self.y - other.y,
            }
        }

        /// manhattan distance
        pub fn l1_dist(&self, other: &Self) -> u64 {
            ((self.x - other.x).abs() + (self.y - other.y).abs()) as u64
        }
    }

    #[derive(Debug, Clone)]
    pub struct LineSegment {
        pub from: Point2d,
        pub to: Point2d,
    }

    impl LineSegment {
        pub fn new(from: Point2d, to: Point2d) -> LineSegment {
            if from.x != to.x && from.y != to.y {
                panic!(
                    "Points {:?} and {:?} don't lie on a vertical or horizontal line segment",
                    from, to
                )
            } else {
                LineSegment { from, to }
            }
        }
        pub fn length(&self) -> u64 {
            self.from.sub(&self.to).l1_dist(&Point2d::origin())
        }

        pub fn contains(&self, pt: &Point2d) -> bool {
            (pt.x - self.from.x).signum() * (pt.x - self.to.x).signum() <= 0
                && (pt.y - self.from.y).signum() * (pt.y - self.to.y).signum() <= 0
        }

        pub fn intersect(&self, other: &Self) -> Intersection {
            let lb_x = max(min(self.from.x, self.to.x), min(other.from.x, other.to.x));
            let lb_y = max(min(self.from.y, self.to.y), min(other.from.y, other.to.y));
            let ub_x = min(max(self.from.x, self.to.x), max(other.from.x, other.to.x));
            let ub_y = min(max(self.from.y, self.to.y), max(other.from.y, other.to.y));

            match (lb_x.cmp(&ub_x), lb_y.cmp(&ub_y)) {
                (Ordering::Greater, _) | (_, Ordering::Greater) => Intersection::Empty,
                (Ordering::Equal, Ordering::Equal) => {
                    Intersection::Point(Point2d { x: lb_x, y: lb_y })
                }
                _ => unimplemented!(),
            }
        }
    }

    pub enum Intersection {
        Segment(LineSegment),
        Point(Point2d),
        Empty,
    }
}
