use nano_factory::*;

pub const PUZZLE_INPUT: &str = include_str!("../inputs/14.txt");

pub fn main() {
    part1();
    part2();
}

fn parse_input(input: &str) -> Option<RecipeBook> {
    let mut recipe_book = RecipeBook::new();

    for line in input.trim().lines() {
        let (target, recipe) = Recipe::from_string(line)?;
        recipe_book.add(&target, recipe);
    }

    Some(recipe_book)
}

fn part1() {
    if let Some(recipe_book) = parse_input(PUZZLE_INPUT) {
        println!(
            "Part 1: {}",
            Inventory::new().produce(vec![(Chemical::new("FUEL"), 1)], &recipe_book)
        );
    }
}

// HACK: avoids overflow, but still large enough to get correct answer
const MAX_EXPONENT: u64 = 32;

fn part2() {
    let cargo_hold: Amount = 1_000_000_000_000;

    if let Some(recipe_book) = parse_input(PUZZLE_INPUT) {
        let mut fuel_request = 0;
        // binary search for maximal `fuel_request` servicable by the inventory
        for bit in (0..MAX_EXPONENT).rev() {
            let ore_needed = Inventory::new().produce(
                vec![(Chemical::new("FUEL"), fuel_request + (1 << bit))],
                &recipe_book,
            );
            if ore_needed <= cargo_hold {
                fuel_request += 1 << bit;
            }
        }
        println!("Part 2: {}", fuel_request);
    }
}

mod nano_factory {
    use std::collections::{HashMap, VecDeque};

    pub type Amount = u64;

    #[derive(Debug, PartialEq, Eq, Hash, Clone)]
    pub struct Chemical(String);

    impl Chemical {
        pub fn new(name: &str) -> Chemical {
            Chemical(name.trim().to_owned())
        }

        fn parse_amount(input: &str) -> Option<(Amount, Chemical)> {
            let mut iter = input.trim().split(' ');
            let amount = iter.next()?.trim().parse().ok()?;
            let code = iter.next()?;
            Some((amount, Chemical::new(code)))
        }
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct Inventory(HashMap<Chemical, Amount>);

    impl Inventory {
        pub fn new() -> Inventory {
            Inventory(HashMap::new())
        }
        pub fn ask(&mut self, chemical: &Chemical, amount: Amount) -> Amount {
            if let Some(&available) = self.0.get(&chemical) {
                if available <= amount {
                    self.0.remove(&chemical);
                    available
                } else {
                    self.0.insert(chemical.clone(), available - amount);
                    amount
                }
            } else {
                0
            }
        }

        pub fn store(&mut self, chemical: &Chemical, amount: Amount) {
            if amount > 0 {
                match self.0.get(chemical) {
                    Some(&available) => self.0.insert(chemical.clone(), available + amount),
                    None => self.0.insert(chemical.clone(), amount),
                };
            }
        }

        pub fn produce(
            &mut self,
            tasks: Vec<(Chemical, Amount)>,
            recipe_book: &RecipeBook,
        ) -> Amount {
            let ore = Chemical::new("ORE");
            let mut tasks = VecDeque::from(tasks);
            let mut fuel_requirement = 0;

            while let Some((task, mut task_amount)) = tasks.pop_front() {
                if task == ore {
                    fuel_requirement += task_amount;
                } else {
                    // get available chemicals from the inventory
                    task_amount -= self.ask(&task, task_amount);
                    let recipe = recipe_book.get(&task).unwrap();
                    let remainder = if task_amount % recipe.output > 0 {
                        1
                    } else {
                        0
                    };
                    let num_repeat = (task_amount / recipe.output) + remainder;
                    // queue up dependencies
                    for (ingredient, ingredient_amount) in recipe.required.clone() {
                        tasks.push_back((ingredient, ingredient_amount * num_repeat))
                    }
                    // add excess to inventory
                    let excess = num_repeat * recipe.output - task_amount;
                    self.store(&task, excess);
                }
            }

            fuel_requirement
        }
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct Recipe {
        pub output: Amount,
        pub required: HashMap<Chemical, Amount>,
    }

    impl Recipe {
        pub fn from_string(input: &str) -> Option<(Chemical, Recipe)> {
            let mut iter = input.split("=>");
            let lhs = iter.next()?;
            let rhs = iter.next()?;
            let mut required = HashMap::<Chemical, Amount>::new();
            for ingredient in lhs.split(',') {
                let (ingredient_amount, ingredient) = Chemical::parse_amount(ingredient)?;
                required.insert(ingredient, ingredient_amount);
            }
            let (output, target) = Chemical::parse_amount(rhs)?;

            Some((target, Recipe { required, output }))
        }
    }

    #[derive(Debug, PartialEq, Eq, Clone)]
    pub struct RecipeBook(HashMap<Chemical, Recipe>);

    impl RecipeBook {
        pub fn new() -> RecipeBook {
            RecipeBook(HashMap::new())
        }

        pub fn add(&mut self, chemical: &Chemical, recipe: Recipe) {
            self.0.insert(chemical.clone(), recipe);
        }

        pub fn get(&self, chemical: &Chemical) -> Option<&Recipe> {
            self.0.get(chemical)
        }
    }
}
