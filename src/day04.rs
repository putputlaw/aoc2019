use rayon::prelude::*;
use std::cmp::{max, min};

pub fn main() {
    part1();
    part2();
}

fn get_input() -> Vec<usize> {
    let raw = include_str!("../inputs/04.txt");
    raw.trim()
        .split('-')
        .flat_map(|s| s.parse::<usize>())
        .collect()
}

fn part1() {
    let input = get_input();
    // rule 1 & 2: normalise range for 6-digit numbers
    let range = max(100_000, input[0])..=min(999_999, input[1]);
    println!(
        "Answer for Part 1: {:?}",
        range
            .map(|n| (0..6)
                .map(|i| (n / 10usize.pow(i)) % 10)
                .rev()
                .collect::<Vec<_>>())
            // rule 3
            .filter(|digits| is_sorted(digits))
            // rule 4
            .filter(|digits| {
                let mut digits = digits.clone();
                digits.dedup();
                digits.len() < 6
            })
            .count()
    );
}

fn part2() {
    let input = get_input();
    // rule 1 & 2: normalise range for 6-digit numbers
    let range = max(100_000, input[0])..=min(999_999, input[1]);
    println!(
        "Answer for Part 2: {:?}",
        range
            .into_par_iter()
            .map(|n| (0..6)
                .map(|i| (n / 10usize.pow(i)) % 10)
                .rev()
                .collect::<Vec<_>>())
            // rule 3
            .filter(|digits| is_sorted(digits))
            // rule 4
            .filter(|digits| {
                let mut digits = digits.clone();
                digits.dedup();
                digits.len() < 6
            })
            // rule 5
            .filter(|digits| {
                let (_, last_count, mut sig) =
                    digits
                        .iter()
                        .fold((0, 0, vec![]), |(key, count, mut sig), new| {
                            if key == *new {
                                (key, count + 1, sig)
                            } else {
                                sig.push(count);
                                (*new, 1, sig)
                            }
                        });
                sig.push(last_count);
                sig.contains(&2)
            })
            .count()
    );
}

fn is_sorted<T: Ord + Clone>(list: &[T]) -> bool {
    let mut copy = list.to_vec();
    copy.sort();
    copy == list
}
