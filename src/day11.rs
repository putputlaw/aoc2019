use crate::intcode::*;
use bot::*;
use std::collections::HashMap;

const PUZZLE_INPUT: &str = include_str!("../inputs/11.txt");

pub fn main() {
    part1();
    part2();
}

fn get_input() -> IntCodeMachine {
    IntCodeMachine::from_string(PUZZLE_INPUT)
}

fn part1() {
    let panels = paint(Color::Black);
    println!("Part 1: {}", panels.len())
}

fn paint(initial_color: Color) -> HashMap<(i32, i32), Color> {
    let mut program = get_input();
    program.inputs.push(initial_color.to_i64());
    let mut bot = Bot::default();
    while let (Some(col), Some(rot)) = (program.next_output(), program.next_output()) {
        match col {
            0 => bot.paint(Color::Black),
            1 => bot.paint(Color::White),
            _ => panic!("unknown color"),
        }

        match rot {
            0 => bot.turn_left(),
            1 => bot.turn_right(),
            _ => panic!("unknown turn"),
        }

        bot.forward();

        program.inputs.push(bot.color().to_i64());
    }
    bot.panels
}

fn part2() {
    let mut panels = paint(Color::White);

    let (minx, miny, maxx, maxy) = panels
        .keys()
        .fold((0, 0, 0, 0), |(minx, miny, maxx, maxy), &(x, y)| {
            (minx.min(x), miny.min(y), maxx.max(x), maxy.max(y))
        });

    println!("Part 2:");
    for y in miny..=maxy {
        for x in minx..=maxx {
            match panels.entry((x, maxy + miny - y)).or_insert(Color::Black) {
                Color::Black => print!("　"),
                Color::White => print!("色"),
            }
        }
        println!();
    }
}

pub mod bot {
    use std::collections::HashMap;

    pub struct Bot {
        pub pos: (i32, i32),
        pub dir: Direction,
        pub panels: HashMap<(i32, i32), Color>,
    }

    impl Bot {
        pub fn paint(&mut self, color: Color) {
            self.panels.insert(self.pos, color);
        }

        pub fn turn_left(&mut self) {
            self.dir = self.dir.turn_left();
        }

        pub fn turn_right(&mut self) {
            self.dir = self.dir.turn_right();
        }

        pub fn forward(&mut self) {
            match self.dir {
                Direction::Up => self.pos.1 += 1,
                Direction::Right => self.pos.0 += 1,
                Direction::Down => self.pos.1 -= 1,
                Direction::Left => self.pos.0 -= 1,
            }
        }
        pub fn color(&self) -> Color {
            self.panels.get(&self.pos).copied().unwrap_or(Color::Black)
        }
    }

    impl Default for Bot {
        fn default() -> Self {
            Bot {
                pos: (0, 0),
                dir: Direction::Up,
                panels: HashMap::new(),
            }
        }

    }

    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub enum Color {
        Black,
        White,
    }

    impl Color {
        pub fn to_i64(self) -> i64 {
            match self {
                Color::Black => 0,
                Color::White => 1,
            }
        }
    }

    #[derive(Copy, Clone, Debug, PartialEq, Eq)]
    pub enum Direction {
        Up,
        Right,
        Down,
        Left,
    }

    impl Direction {
        fn turn_left(self) -> Self {
            match self {
                Direction::Up => Direction::Left,
                Direction::Right => Direction::Up,
                Direction::Down => Direction::Right,
                Direction::Left => Direction::Down,
            }
        }

        fn turn_right(self) -> Self {
            match self {
                Direction::Up => Direction::Right,
                Direction::Right => Direction::Down,
                Direction::Down => Direction::Left,
                Direction::Left => Direction::Up,
            }
        }
    }
}
