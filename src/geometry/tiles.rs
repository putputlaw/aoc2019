use crate::geometry::point::Point;
use crate::geometry::rectangle::Rectangle;
use std::collections::HashMap;
use std::iter::FromIterator;

#[derive(Clone, Debug)]
pub struct Tiles<T> {
    tile_set: HashMap<Point, T>,
}

impl<T> Tiles<T> {
    pub fn new(width: isize, height: isize, default: T) -> Self
    where
        T: Clone,
    {
        Tiles {
            tile_set: Rectangle::Closed {
                min_x: 0,
                max_x: width,
                min_y: -height,
                max_y: 0,
            }
            .iter()
            .map(|pt| (pt, default.clone()))
            .collect(),
        }
    }
    pub fn from_string<F>(input: &str, mut f: F) -> Self
    where
        F: FnMut(Point, char) -> Option<T>,
        T: Clone,
    {
        let mut tile_set = HashMap::new();

        for (row, line) in input.lines().enumerate() {
            for (col, ch) in line.chars().enumerate() {
                let pt = (col as isize, -(row as isize));
                if let Some(tile) = f(pt, ch) {
                    tile_set.insert(pt, tile);
                }
            }
        }
        Tiles { tile_set }
    }

    pub fn get(&self, pt: Point) -> Option<T>
    where
        T: Clone,
    {
        self.tile_set.get(&pt).cloned()
    }

    pub fn insert(&mut self, pt: Point, tile: T) -> Option<T> {
        self.tile_set.insert(pt, tile)
    }

    pub fn get_many(&self, pts: &[Point]) -> Vec<(Point, T)>
    where
        T: Clone,
    {
        pts.iter()
            .flat_map(|pt| self.tile_set.get(pt).map(|tile| (*pt, tile.clone())))
            .collect()
    }

    pub fn keys(&self) -> Vec<Point> {
        self.tile_set.keys().cloned().collect()
    }

    pub fn values(&self) -> Vec<&T> {
        self.tile_set.values().collect()
    }

    pub fn to_vec(&self) -> Vec<(Point, T)>
    where
        T: Clone,
    {
        self.tile_set
            .iter()
            .map(|(pt, tile)| (*pt, tile.clone()))
            .collect()
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = (&Point, &T)> + 'a {
        self.tile_set.iter()
    }
}

impl<T> FromIterator<(Point, T)> for Tiles<T> {
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = (Point, T)>,
    {
        Tiles {
            tile_set: iter.into_iter().collect(),
        }
    }
}
