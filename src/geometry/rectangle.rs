use super::point::*;
use Rectangle::*;
use std::ops::RangeInclusive;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Rectangle {
    Empty,
    Closed {
        min_x: isize,
        min_y: isize,
        max_x: isize,
        max_y: isize,
    },
}

impl Rectangle {
    pub fn empty() -> Self {
        Rectangle::Empty
    }

    pub fn grow(&self, pt: Point) -> Self {
        match self {
            Empty => Closed {
                min_x: pt.0,
                min_y: pt.1,
                max_x: pt.0,
                max_y: pt.1,
            },
            Closed {
                min_x,
                min_y,
                max_x,
                max_y,
            } => Closed {
                min_x: *min_x.min(&pt.0),
                min_y: *min_y.min(&pt.1),
                max_x: *max_x.max(&pt.0),
                max_y: *max_y.max(&pt.1),
            },
        }
    }

    pub fn grow_many(&self, pts: &[Point]) -> Self {
        let mut rect = *self;
        for pt in pts {
            rect = rect.grow(*pt)
        }
        rect
    }

    pub fn interior(&self) -> Self {
        match self {
            Empty => Empty,
            Closed {
                min_x,
                min_y,
                max_x,
                max_y,
            } => {
                if *min_x + 2 < *max_x && *min_y + 2 <= *max_y {
                    Closed {
                        min_x: *min_x + 1,
                        min_y: *min_y + 1,
                        max_x: *max_x - 1,
                        max_y: *max_y - 1,
                    }
                } else {
                    Empty
                }
            }
        }
    }

    pub fn contains(&self, pt: Point) -> bool {
        match self {
            Empty => false,
            Closed {
                min_x,
                min_y,
                max_x,
                max_y,
            } => (*min_x..=*max_x).contains(&pt.0) && (*min_y..=*max_y).contains(&pt.1),
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = Point> {
        match self {
            Empty => iproduct!(0..=-1, 0..=-1),
            Closed {
                min_x,
                min_y,
                max_x,
                max_y,
            } => iproduct!(*min_x..=*max_x, *min_y..=*max_y),
        }
    }

    pub fn width(&self) -> usize {
        match self {
            Empty => 0,
            Closed { min_x, max_x, .. } => (max_x - min_x + 1) as usize,
        }
    }

    pub fn height(&self) -> usize {
        match self {
            Empty => 0,
            Closed { min_y, max_y, .. } => (max_y - min_y + 1) as usize,
        }
    }

    pub fn x_range(&self) -> RangeInclusive<isize> {
        match self {
            Empty => 0..=-1,
            Closed {min_x, max_x, ..} => *min_x..=*max_x
        }
    }

    pub fn y_range(&self) -> RangeInclusive<isize> {
        match self {
            Empty => 0..=-1,
            Closed {min_y, max_y, ..} => *min_y..=*max_y
        }
    }
}
