use core::hash::Hash;
use core::ops::*;

#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy)]
pub struct V3<T>(pub T, pub T, pub T);

impl<T> V3<T> {
    pub fn map<F, S>(&self, f: F) -> V3<S>
    where
        F: Fn(T) -> S,
        T: Clone,
    {
        match self {
            V3(a, b, c) => V3(f(a.clone()), f(b.clone()), f(c.clone())),
        }
    }
}

impl V3<i64> {
    pub fn from_string(input: &str) -> Option<V3<i64>> {
        use regex::Regex;
        let re = Regex::new(r"<x=(-?\d+),\s*y=(-?\d+),\s*z=(-?\d+)\s*>").ok()?;
        let cap = re.captures_iter(input).next()?;
        Some(V3(
            cap[1].parse().ok()?,
            cap[2].parse().ok()?,
            cap[3].parse().ok()?,
        ))
    }

    pub fn l1_norm(&self) -> u64 {
        (self.0.abs() + self.1.abs() + self.2.abs()) as u64
    }
}

impl<T> Add for V3<T>
where
    T: Add<Output = T>,
{
    type Output = Self;
    fn add(self, other: Self) -> Self {
        V3(self.0 + other.0, self.1 + other.1, self.2 + other.2)
    }
}

impl<T> Sub for V3<T>
where
    T: Sub<Output = T>,
{
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        V3(self.0 - other.0, self.1 - other.1, self.2 - other.2)
    }
}
