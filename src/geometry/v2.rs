use core::hash::Hash;
use core::ops::*;

#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy)]
pub struct V2<T>(pub T, pub T);

impl<T> V2<T> {
    pub fn map<F, S>(&self, f: F) -> V2<S>
    where
        F: Fn(T) -> S,
        T: Clone,
    {
        match self {
            V2(a, b) => V2(f(a.clone()), f(b.clone())),
        }
    }
}

impl V2<i64> {
    pub fn from_string(input: &str) -> Option<V2<i64>> {
        use regex::Regex;
        let re = Regex::new(r"<x=(-?\d+),\s*y=(-?\d+)\s*>").ok()?;
        let cap = re.captures_iter(input).next()?;
        Some(V2(
            cap[1].parse().ok()?,
            cap[2].parse().ok()?,
        ))
    }

    pub fn l1_norm(&self) -> u64 {
        (self.0.abs() + self.1.abs()) as u64
    }
}

impl<T> Add for V2<T>
where
    T: Add<Output = T>,
{
    type Output = Self;
    fn add(self, other: Self) -> Self {
        V2(self.0 + other.0, self.1 + other.1)
    }
}

impl<T> Sub for V2<T>
where
    T: Sub<Output = T>,
{
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        V2(self.0 - other.0, self.1 - other.1)
    }
}
