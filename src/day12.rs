use crate::geometry::v3::V3;
use std::collections::HashSet;

pub const PUZZLE_INPUT: &str = include_str!("../inputs/12.txt");

pub fn main() {
    part1();
    part2();
}

pub fn get_input() -> Vec<Moon> {
    PUZZLE_INPUT
        .trim()
        .lines()
        .flat_map(|line| V3::from_string(line))
        .map(Moon::new)
        .collect()
}

pub fn part1() {
    let mut moons = get_input();
    for _ in 0..1000 {
        evolve(&mut moons);
    }
    println!(
        "Part 1: {}",
        moons.iter().map(|moon| moon.energy()).sum::<u64>()
    );
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Hash)]
pub struct Moon {
    pub pos: V3<i64>,
    pub vel: V3<i64>,
}

impl Moon {
    pub fn new(pos: V3<i64>) -> Moon {
        Moon {
            pos,
            vel: V3(0, 0, 0),
        }
    }
    pub fn apply_gravity(&mut self, moons: &[Moon]) {
        for moon in moons {
            self.vel = self.vel + (moon.pos - self.pos).map(|x| x.signum())
        }
    }

    pub fn apply_velocity(&mut self) {
        self.pos = self.pos + self.vel;
    }

    pub fn energy(&self) -> u64 {
        self.pos.l1_norm() * self.vel.l1_norm()
    }
}

fn evolve(moons: &mut Vec<Moon>) {
    let moons = &mut moons[..];
    let freeze = moons.to_vec();
    for moon in moons.iter_mut() {
        moon.apply_gravity(&freeze);
        moon.apply_velocity();
    }
}

pub fn part2() {
    let moons = get_input();
    // simulate each coordinate separately, then compute smallest common multiple
    // for 100% correctness, I should probably compute (precycle length, cycle length), but I assume
    // precycle length = 0, otherwise the problem gets a bit harder :)
    let nx = find_repeat(
        moons
            .iter()
            .map(|m| Moon::new(V3(m.pos.0, 0, 0)))
            .collect(),
    );

    let ny = find_repeat(
        moons
            .iter()
            .map(|m| Moon::new(V3(0, m.pos.1, 0)))
            .collect(),
    );

    let nz = find_repeat(
        moons
            .iter()
            .map(|m| Moon::new(V3(0, 0, m.pos.2)))
            .collect(),
    );

    use num::integer::lcm;

    println!("Part 2: {}", lcm(nx, lcm(ny, nz)))
}

fn find_repeat(mut moons: Vec<Moon>) -> u64 {
    let mut history = HashSet::new();
    let mut steps = 0;

    while !history.contains(&moons) {
        history.insert(moons.clone());
        evolve(&mut moons);
        steps += 1;
    }
    steps
}
