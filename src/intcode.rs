use std::collections::{HashMap, VecDeque};

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct IntCodeMachine {
    pub mem: HybridMemory,
    pub ip: i64,
    pub rel_base: i64,
    pub inputs: InputSupply,
}

/// more descriptive way of saying Option<i64>
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Step {
    Silent,
    Output(i64),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Mode {
    Pos, // position
    Im,  // immediate
    Rel, // relative
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Op {
    Add,  // add
    Mul,  // multiply
    In,   // input
    Out,  // output
    Ju,   // jump if true
    Jz,   // jump if false
    Lt,   // less than
    Eq,   // equals
    Rbo,  // relative base offset
    Halt, // halt
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct InputSupply {
    pub queue: VecDeque<i64>,
    pub fallback: Option<i64>,
}

impl InputSupply {
    pub fn push(&mut self, i: i64) {
        self.queue.push_back(i);
    }

    pub fn push_vec(&mut self, is: Vec<i64>) {
        for i in is {
            self.queue.push_back(i);
        }
    }

    pub fn pop(&mut self) -> Option<i64> {
        if self.queue.is_empty() {
            self.fallback
        } else {
            self.queue.pop_front()
        }
    }
}

impl IntCodeMachine {
    pub fn new(mem: Vec<i64>) -> IntCodeMachine {
        IntCodeMachine {
            mem: HybridMemory::from_vec(mem),
            ip: 0,
            rel_base: 0,
            inputs: InputSupply {
                queue: VecDeque::new(),
                fallback: None,
            },
        }
    }

    pub fn from_string(code: &str) -> IntCodeMachine {
        IntCodeMachine::new(code.trim().split(',').flat_map(|n| n.parse()).collect())
    }

    pub fn run(&mut self) -> Vec<i64> {
        self.flat_map(|step| match step {
            Step::Output(o) => Some(o),
            _ => None,
        })
        .collect()
    }

    pub fn next_output(&mut self) -> Option<i64> {
        loop {
            match self.next() {
                Some(Step::Output(o)) => return Some(o),
                None => return None,
                _ => {}
            }
        }
    }

    fn put(&mut self, arg: u32, value: i64) {
        let location = self.ip + arg as i64;
        match self.mode(arg) {
            Mode::Pos => self.mem.put(self.mem.get(location), value),
            Mode::Rel => self.mem.put(self.mem.get(location) + self.rel_base, value),
            Mode::Im => panic!("cannot put in immediate mode"),
        };
    }

    fn get(&self, arg: u32) -> i64 {
        let location = self.ip + arg as i64;
        match self.mode(arg) {
            Mode::Pos => self.mem.get(self.mem.get(location)),
            Mode::Im => self.mem.get(location),
            Mode::Rel => self.mem.get(self.rel_base + self.mem.get(location)),
        }
    }

    fn mode(&self, arg: u32) -> Mode {
        let val = self.mem.get(self.ip);
        match (val / 10_i64.pow(1 + arg)) % 10 {
            0 => Mode::Pos,
            1 => Mode::Im,
            2 => Mode::Rel,
            _ => panic!("invalid mode"),
        }
    }

    pub fn with_fallback_input(mut self, value: i64) -> Self {
        self.inputs.fallback = Some(value);
        self
    }

    pub fn with_input(mut self, value: Vec<i64>) -> Self {
        self.inputs.queue = VecDeque::from(value);
        self
    }

    pub fn with_noun_and_verb(mut self, noun: i64, verb: i64) -> Self {
        self.mem.put(1, noun);
        self.mem.put(2, verb);
        self
    }

    pub fn is_halted(&self) -> bool {
        parse_opcode(self.mem.get(self.ip)).0 == Op::Halt
    }
}

impl Iterator for IntCodeMachine {
    type Item = Step;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result = Some(Step::Silent);
        let (opcode, length) = parse_opcode(self.mem.get(self.ip));
        match opcode {
            Op::Add => self.put(3, self.get(1) + self.get(2)),
            Op::Mul => self.put(3, self.get(1) * self.get(2)),
            Op::In => match self.inputs.pop() {
                Some(input) => {
                    self.put(1, input);
                    self.ip += 2;
                }
                None => result = None,
            },
            Op::Out => result = Some(Step::Output(self.get(1))),
            Op::Ju => {
                if self.get(1) != 0 {
                    self.ip = self.get(2);
                } else {
                    self.ip += 3;
                }
            }
            Op::Jz => {
                if self.get(1) == 0 {
                    self.ip = self.get(2);
                } else {
                    self.ip += 3;
                }
            }
            Op::Lt => self.put(3, if self.get(1) < self.get(2) { 1 } else { 0 }),
            Op::Eq => self.put(3, if self.get(1) == self.get(2) { 1 } else { 0 }),
            Op::Rbo => self.rel_base += self.get(1),
            Op::Halt => result = None,
        }

        self.ip += length as i64;

        result
    }
}

fn parse_opcode(code: i64) -> (Op, usize) {
    use Op::*;
    let (opcode, length) = match code % 100 {
        1 => (Add, 4),
        2 => (Mul, 4),
        3 => (In, 0), // NOTE: special handling
        4 => (Out, 2),
        5 => (Ju, 0), // NOTE: special handling
        6 => (Jz, 0), // NOTE: special handling
        7 => (Lt, 4),
        8 => (Eq, 4),
        9 => (Rbo, 2),
        99 => (Halt, 0),
        _ => panic!("invalid opcode"),
    };
    (opcode, length)
}

// TODO: we have to consider for (Partial)Eq instance that there are different representations of the same memory (e.g. map[n] = 0 vs. map[n] = undefined)
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct HybridMemory {
    arr: Vec<i64>,
    map: HashMap<i64, i64>,
}

impl HybridMemory {
    pub fn get(&self, index: i64) -> i64 {
        if (0..self.arr.len()).contains(&(index as usize)) {
            self.arr[index as usize]
        } else {
            self.map.get(&index).cloned().unwrap_or(0)
        }
    }

    pub fn put(&mut self, index: i64, value: i64) {
        if (0..self.arr.len()).contains(&(index as usize)) {
            self.arr[index as usize] = value;
        } else {
            self.map.insert(index, value);
        }
    }

    pub fn from_vec(arr: Vec<i64>) -> Self {
        HybridMemory {
            arr,
            map: HashMap::new(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_day_2() {
        let test_cases = vec![
            (vec![1, 0, 0, 0, 99], vec![2, 0, 0, 0, 99]),
            (vec![2, 3, 0, 3, 99], vec![2, 3, 0, 6, 99]),
            (vec![2, 4, 4, 5, 99, 0], vec![2, 4, 4, 5, 99, 9801]),
            (
                vec![1, 1, 1, 4, 99, 5, 6, 0, 99],
                vec![30, 1, 1, 4, 2, 5, 6, 0, 99],
            ),
        ];

        for (lhs, rhs) in test_cases {
            let mut prog_lhs = IntCodeMachine::new(lhs);
            prog_lhs.run();
            let prog_rhs = IntCodeMachine::new(rhs);
            assert_eq!(prog_lhs.mem, prog_rhs.mem);
        }
    }

    #[test]
    fn test_1() {
        let prog = vec![
            109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99,
        ];
        assert_eq!(IntCodeMachine::new(prog.clone()).run(), prog);
    }

    #[test]
    fn test_2() {
        let prog = vec![104, 1125899906842624, 99];
        assert_eq!(IntCodeMachine::new(prog).run(), vec![1125899906842624]);
    }

    #[test]
    fn test_3() {
        let prog = vec![1102, 34915192, 34915192, 7, 4, 7, 99, 0];
        assert_eq!(
            IntCodeMachine::new(prog.clone()).run(),
            vec![1219070632396864]
        );
    }

    #[test]
    fn test_add() {
        for (op_code, result) in vec![(1101, 30), (0101, 10), (1001, 20), (0001, 0)] {
            let mut prog = IntCodeMachine::new(vec![op_code, 10, 20, 3, 99]);
            prog.next();
            assert_eq!(prog.mem.get(3), result);
        }
    }

    #[test]
    fn test_mul() {
        for (op_code, result) in vec![(1102, 30), (0102, 55), (1002, 42), (0002, 77)] {
            let mut prog = IntCodeMachine::new(vec![op_code, 5, 6, 3, 99, 7, 11]);
            prog.next();
            assert_eq!(prog.mem.get(3), result);
        }
    }

    #[test]
    fn test_equal_pos_mode() {
        for (input, output) in vec![(8, 1), (2, 0)] {
            let code = vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8];
            let outputs = IntCodeMachine::new(code).with_fallback_input(input).run();
            println!("{:?}", outputs);
            assert_eq!(outputs, vec![output]);
        }
    }

    #[test]
    fn test_less_pos_mode() {
        for (input, output) in vec![(7, 1), (8, 0)] {
            let code = vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8];
            let outputs = IntCodeMachine::new(code).with_fallback_input(input).run();
            println!("{:?}", outputs);
            assert_eq!(outputs, vec![output]);
        }
    }

    #[test]
    fn test_equal_im_mode() {
        for (input, output) in vec![(8, 1), (2, 0)] {
            let code = vec![3, 3, 1108, -1, 8, 3, 4, 3, 99];
            let outputs = IntCodeMachine::new(code).with_fallback_input(input).run();
            println!("{:?}", outputs);
            assert_eq!(outputs, vec![output]);
        }
    }

    #[test]
    fn test_less_im_mode() {
        for (input, output) in vec![(7, 1), (8, 0)] {
            let code = vec![3, 3, 1107, -1, 8, 3, 4, 3, 99];
            let outputs = IntCodeMachine::new(code).with_fallback_input(input).run();
            println!("{:?}", outputs);
            assert_eq!(outputs, vec![output]);
        }
    }

    #[test]
    fn test_jump() {
        for (input, output) in vec![(0, 0), (3, 1)] {
            let code = vec![3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9];
            let outputs = IntCodeMachine::new(code).with_fallback_input(input).run();
            println!("{:?}", outputs);
            assert_eq!(outputs, vec![output]);
        }
    }

    #[test]
    fn test_jumpz() {
        for (input, output) in vec![(0, 0), (3, 1)] {
            let code = vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1];
            let outputs = IntCodeMachine::new(code).with_fallback_input(input).run();
            println!("{:?}", outputs);
            assert_eq!(outputs, vec![output]);
        }
    }

    #[test]
    fn test_big() {
        for (input, output) in vec![(7, 999), (8, 1000), (9, 1001)] {
            let code = vec![
                3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36,
                98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000,
                1, 20, 4, 20, 1105, 1, 46, 98, 99,
            ];
            let outputs = IntCodeMachine::new(code).with_fallback_input(input).run();
            println!("{:?}", outputs);
            assert_eq!(outputs, vec![output]);
        }
    }

    #[test]
    fn test_day5_part2() {
        for (input, output) in vec![(1, 7566643), (5, 9265694)] {
            let code: Vec<i64> = include_str!("../inputs/05.txt")
                .trim()
                .split(",")
                .flat_map(|n| n.parse())
                .collect();
            let outputs = IntCodeMachine::new(code).with_fallback_input(input).run();
            println!("{:?}", outputs);
            assert_eq!(outputs.last().map(|n| *n), Some(output));
        }
    }

    #[test]
    fn test_input_relative() {
        for (input, output) in vec![(12, 12)] {
            let code = vec![109, -1, 203, 2, 204, 2, 99];
            let outputs = IntCodeMachine::new(code).with_fallback_input(input).run();
            println!("{:?}", outputs);
            assert_eq!(outputs, vec![output]);
        }
    }
}
