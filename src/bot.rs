#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum CardDir {
    North,
    East,
    South,
    West,
}

impl CardDir {
    pub fn turn_left(self) -> Self {
        match self {
            CardDir::North => CardDir::West,
            CardDir::East => CardDir::North,
            CardDir::South => CardDir::East,
            CardDir::West => CardDir::South,
        }
    }

    pub fn turn_right(self) -> Self {
        match self {
            CardDir::North => CardDir::East,
            CardDir::East => CardDir::South,
            CardDir::South => CardDir::West,
            CardDir::West => CardDir::North,
        }
    }

    pub fn apply(self, (row, col): (i64, i64)) -> (i64, i64) {
        match self {
            CardDir::North => (row - 1, col),
            CardDir::East => (row, col + 1),
            CardDir::South => (row + 1, col),
            CardDir::West => (row, col - 1),
        }
    }
}
