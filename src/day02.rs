use crate::intcode::*;

pub fn main() {
    part1();
    part2();
}

fn get_input() -> IntCodeMachine {
    IntCodeMachine::from_string(include_str!("../inputs/02.txt"))
}

fn part1() {
    let mut program = get_input().with_noun_and_verb(12, 2);
    program.run();
    println!("Part 1: {}", program.mem.get(0));
}

fn part2() {
    use std::cmp::Ordering;

    let program = get_input();
    let target = 19_690_720;
    // use the fact that the (limited) int code machine is monotonic in its inputs
    let mut noun = 0;
    let mut verb = 0;
    loop {
        let mut program = program.clone().with_noun_and_verb(noun, verb);
        program.run();
        // here we assume that a change in the noun has a greater effect than a change in the verb
        match program.mem.get(0).cmp(&target) {
            Ordering::Less => noun += 1,
            Ordering::Greater => {
                verb += 1;
                noun -= 1;
            }
            Ordering::Equal => return println!("Part 2: {}", noun * 100 + verb),
        };
    }
}
