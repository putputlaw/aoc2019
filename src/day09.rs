use crate::intcode::*;

pub fn main() {
    part1();
    part2();
}

fn get_input() -> IntCodeMachine {
    IntCodeMachine::from_string(include_str!("../inputs/09.txt"))
}

fn part1() {
    let program = get_input();
    let outputs = program.with_fallback_input(1).run();
    println!("{}", &outputs[0]);
}

fn part2() {
    let program = get_input();
    let outputs = program.with_fallback_input(2).run();
    println!("{}", &outputs[0]);
}
