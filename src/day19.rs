use crate::intcode::*;

const PUZZLE_INPUT: &str = include_str!("../inputs/19.txt");

pub fn main() {
    let program = parse_input(PUZZLE_INPUT);
    println!("Part 1: {}", part1(&program));
    let (x, y) = part2(&program);
    println!("Part 2: {}", x * 10_000 + y);
}

pub fn parse_input(input: &str) -> IntCodeMachine {
    IntCodeMachine::from_string(input)
}

pub fn part1(program: &IntCodeMachine) -> i64 {
    iproduct!(0..50, 0..50)
        .filter(|(x, y)| is_beam(program, *x, *y))
        .count() as i64
}

fn is_beam(program: &IntCodeMachine, x: i64, y: i64) -> bool {
    let mut program = program.clone();
    program.inputs.push(x);
    program.inputs.push(y);
    program.next_output().unwrap() == 1
}

pub fn part2(program: &IntCodeMachine) -> (i64, i64) {
    let mut dist = 0;
    let mut x = 0;
    // smarter search
    let mut min_x = 0;
    let mut min_y = 0;
    loop {
        let y = dist - x;
        let top_left = is_beam(program, x, y);
        let bot_right = is_beam(program, x + 99, y + 99);
        let top_right = is_beam(program, x + 99, y);
        let bot_left = is_beam(program, x, y + 99);

        if top_left && top_right && bot_left && bot_right {
            return (x,y)
        }

        // we now narrow down the search area assuming that
        // the zone of attraction is a cone of attraction
        // (i.e. we assume convexity)

        if !bot_left {
            min_x = x+1;
        }

        if !top_right {
            min_y = y+1;
        }

        x += 1;
        if x + min_y > dist {
            x = min_x;
            dist += 1;
        }
    }
}
