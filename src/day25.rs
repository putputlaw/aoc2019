use crate::intcode::*;

// MAP
//
//
// Passages  --  Sick Bay -- Crew Quarters -- Observatory -- Stables -- [Hull Breach]     <Pressuer Sensitive Floor>
//                  |               |             |                                                  |
//                  |               |             |        Hallway    Gift Wraping Center -- Security Checkpoint
//                  |               |             |           |            |
// Warp Drive -- Arcade        Storage         Kitchen -- Holodeck -- Engineering
// Maintenance                                    |                        |
//                             Science Lab -- Navigation                Corridor
//                                                |
//                                      Hot Chocolate Fountain
//
// ITEMS
//
// Stables:                semiconductor
// Observatory:            planetoid
// Crew Quarters:          food ration
// Sick Bay:               fixed point
// Passages:               klein bottle
// Warp Drive Maintenance: weather machine
// Kitchen:                giant electro magnet (The giant electromagnet is stuck to you.  You can't move!!)
// Hot Chocolate Fountain: pointer
// Science Lab:            inifinite loop (...)
// Holodeck:               coin
// Hallway:                escape pod (You're launched into space! Bye!)
// Engineering:            photons (It is suddenly completely dark! You are eaten by a Grue!)
// Corridor:               molten lava (The molten lava is way too hot! You melt!)

const PUZZLE_INPUT: &str = include_str!("../inputs/25.txt");

const COLLECT_ITEMS_AND_GO_TO_CHECKPOINT: &[&str] = &[
    "west",
    "take semiconductor",
    "west",
    "take planetoid",
    "west",
    "take food ration",
    "west",
    "take fixed point",
    "west",
    "take klein bottle",
    "east",
    "south",
    "west",
    "take weather machine",
    "east",
    "north",
    "east",
    "east",
    "south",
    "south",
    "south",
    "take pointer",
    "north",
    "north",
    "east",
    "take coin",
    "east",
    "north",
    "east",
];

const ITEMS: &[&str] = &[
    "food ration",
    "fixed point",
    "weather machine",
    "semiconductor",
    "planetoid",
    "coin",
    "pointer",
    "klein bottle",
];

const SOLUTION: &[&str] = &["food ration", "fixed point", "semiconductor", "planetoid"];

pub fn main() {
    let mut program = IntCodeMachine::from_string(PUZZLE_INPUT);
    program.inputs.push_vec(
        COLLECT_ITEMS_AND_GO_TO_CHECKPOINT
            .join("\n")
            .chars()
            .chain("\n".chars())
            .map(as_i64)
            .collect(),
    );

    program.inputs.push_vec(
        ITEMS
            .iter()
            .map(|item| format!("drop {}", item))
            .collect::<Vec<_>>()
            .join("\n")
            .chars()
            .chain("\n".chars())
            .map(as_i64)
            .collect(),
    );

    program.inputs.push_vec(
        SOLUTION
            .iter()
            .map(|item| format!("take {}", item))
            .collect::<Vec<_>>()
            .join("\n")
            .chars()
            .chain("\n".chars())
            .map(as_i64)
            .collect(),
    );
    program.run();
    program
        .inputs
        .push_vec("north\n".chars().map(as_i64).collect());
    println!("Part 1:");
    for o in program.run() {
        print!("{}", as_char(o));
    }
}

/// used to compute `SOLUTION`, i.e. the right subset of items
pub fn _search() {
    let mut program = IntCodeMachine::from_string(PUZZLE_INPUT);
    program.inputs.push_vec(
        COLLECT_ITEMS_AND_GO_TO_CHECKPOINT
            .join("\n")
            .chars()
            .chain("\n".chars())
            .map(as_i64)
            .collect(),
    );
    program.run();
    let mut subset = 0;
    while !program.is_halted() {
        for o in program.run() {
            print!("{}", as_char(o));
        }
        let mut line = String::new();
        // std::io::stdin().read_line(&mut line).unwrap();
        for (i, item) in ITEMS.iter().enumerate() {
            let bit = 1 == (subset >> i) % 2;
            if bit {
                line.push_str("take ");
            } else {
                line.push_str("drop ");
            }
            line.push_str(item);
            line.push_str("\n");
        }
        line.push_str("inv\n");
        line.push_str("north\n");

        program.inputs.push_vec(line.chars().map(as_i64).collect());
        subset += 1;

        if subset > 2usize.pow(8) {
            panic!("no solution found")
        }
    }
}

fn as_char(num: i64) -> char {
    (num as u8) as char
}

fn as_i64(chr: char) -> i64 {
    (chr as u8) as i64
}
