use itertools::Itertools;

pub fn main() {
    part1();
    part2();
}

type Layer = Vec<Vec<u32>>;
/// a flat layer is obtained from a layer by concatenating all rows
type FlatLayer = Vec<u32>;

const WIDTH: usize = 25;
const HEIGHT: usize = 6;

fn get_input() -> Vec<FlatLayer> {
    include_str!("../inputs/08.txt")
        .chars()
        .flat_map(|c| c.to_digit(10))
        .chunks(WIDTH * HEIGHT)
        .into_iter()
        .map(|flat| flat.collect())
        .collect()
}

fn part1() {
    let flats = get_input();
    if let Some(counts) = flats
        .iter()
        .map(|flat| {
            flat.iter().fold(vec![0, 0, 0], |mut counts, color| {
                counts[*color as usize] += 1;
                counts
            })
        })
        .min_by_key(|counts| counts[0])
    {
        println!("Part 1: {}", counts[1] * counts[2])
    }
}

fn part2() {
    let flats = get_input();
    let transparent: FlatLayer = (0..WIDTH * HEIGHT).map(|_| 2).collect();
    let image: Layer = flats
        .into_iter()
        .fold(transparent, overlay)
        // transform flat layer into layer
        .into_iter()
        .chunks(WIDTH)
        .into_iter()
        .map(|row| row.collect())
        .collect();
    println!("Part 2:");
    for row in image {
        for px in row {
            match px {
                0 => print!("　"),
                1 => print!("星"),
                _ => panic!("invalid pixel"),
            }
        }
        println!();
    }
}

fn overlay(above: FlatLayer, below: FlatLayer) -> FlatLayer {
    above
        .into_iter()
        .zip(below.into_iter())
        .map(|colors| match colors {
            (2, color_below) => color_below,
            (color_above, _) => color_above,
        })
        .collect()
}
