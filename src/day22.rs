const PUZZLE_INPUT: &str = include_str!("../inputs/22.txt");

type Integer = num::BigInt;

pub fn main() {
    let shuffle1 = Shuffle::compile(&parse_input(PUZZLE_INPUT), 10_007);
    println!("Part 1: {}", shuffle1.apply(2019));

    let shuffle2 = Shuffle::compile(&parse_input(PUZZLE_INPUT), 119_3157_1751_4047);
    println!(
        "Part 2: {:?}",
        shuffle2.pow(101_7415_8207_6661).inverse().apply(2020)
    );
}

fn parse_input(input: &str) -> Vec<Shuffle> {
    use regex::Regex;
    input
        .trim()
        .lines()
        .map(|line| {
            let re_c = Regex::new(r"cut (-?\d+)").unwrap();
            let re_i = Regex::new(r"deal with increment (\d+)").unwrap();
            if line == "deal into new stack" {
                Shuffle::Stack
            } else if let Some(cap) = re_c.captures_iter(line).next() {
                Shuffle::Cut(cap[1].parse().unwrap())
            } else if let Some(cap) = re_i.captures_iter(line).next() {
                Shuffle::Increment(cap[1].parse().unwrap())
            } else {
                panic!("parse error")
            }
        })
        .collect()
}

#[derive(Clone, Debug)]
pub struct Linear {
    a: Integer,
    b: Integer,
    modulus: Integer,
}

fn big(n: usize) -> Integer {
    use num::FromPrimitive;
    Integer::from_u64(n as u64).unwrap()
}

fn smol(n: &Integer) -> usize {
    use num::ToPrimitive;
    n.to_usize().unwrap()
}

/// linear map `a`*x + `b` mod `modulus`
impl Linear {
    pub fn identity(modulus: usize) -> Linear {
        Linear::new(1, 0, modulus)
    }

    pub fn new(a: usize, b: usize, modulus: usize) -> Linear {
        Linear {
            a: big(a),
            b: big(b),
            modulus: big(modulus),
        }
    }

    pub fn apply(&self, value: usize) -> usize {
        smol(&(modulo(&(&self.a * &big(value) + &self.b), &self.modulus)))
    }

    pub fn then(&self, other: &Linear) -> Linear {
        assert!(self.modulus == other.modulus);
        Linear {
            a: modulo(&(&self.a * &other.a), &self.modulus),
            b: modulo(&(&self.b * &other.a + &other.b), &self.modulus),
            modulus: self.modulus.clone(),
        }
    }

    pub fn pow(&self, mut e: usize) -> Linear {
        let mut pow = self.clone();
        let mut acc = Linear::identity(smol(&self.modulus));
        while e > 0 {
            if e % 2 == 1 {
                acc = acc.then(&pow);
            }
            pow = pow.then(&pow);
            e /= 2;
        }
        acc
    }

    pub fn inverse(&self) -> Linear {
        use num::integer::{ExtendedGcd, Integer};
        let ExtendedGcd { x, .. } = self.a.extended_gcd(&self.modulus);

        Linear {
            a: x.clone(),
            b: &self.modulus - modulo(&(&self.b * &x), &self.modulus),
            modulus: self.modulus.clone(),
        }
    }
}

fn modulo(a: &Integer, b: &Integer) -> Integer {
    let c = a % b;
    if c < big(0) {
        c + b
    } else {
        c
    }
}

#[derive(Clone, Copy)]
pub enum Shuffle {
    Stack,
    Cut(isize),
    Increment(usize),
}

impl Shuffle {
    pub fn linear(&self, deck_size: usize) -> Linear {
        match self {
            Shuffle::Stack => Linear::new(deck_size - 1, deck_size - 1, deck_size),
            Shuffle::Cut(off) => {
                let b = (-off).rem_euclid(deck_size as isize) as usize;
                Linear::new(1, b, deck_size)
            }
            Shuffle::Increment(inc) => Linear::new(*inc, 0, deck_size),
        }
    }

    pub fn position_after_shuffle(&self, pos: usize, deck_size: usize) -> usize {
        self.linear(deck_size).apply(pos)
    }

    pub fn compile(shuffles: &[Shuffle], deck_size: usize) -> Linear {
        shuffles.iter().fold(Linear::identity(deck_size), |acc, s| {
            acc.then(&s.linear(deck_size))
        })
    }
}
