use priority_queue::*;
use std::collections::*;
use std::hash::Hash;
use std::ops::Add;
use super::reverse_order::ReverseOrder;

pub trait WGraph {
    type Node: Eq + Hash + Clone;
    type Distance: Ord + Add<Output = Self::Distance> + Clone;
    fn neighbours(&self, node: &Self::Node) -> Vec<(Self::Node, Self::Distance)>;
    fn zero() -> Self::Distance;
}

pub struct DijkstraIter<'a, G: WGraph> {
    graph: &'a G,
    visited: HashSet<G::Node>,
    distances: HashMap<G::Node, G::Distance>,
    pq: PriorityQueue<G::Node, ReverseOrder<G::Distance>>,
}

impl<'a, G: WGraph> DijkstraIter<'a, G> {
    pub fn new(graph: &'a G, start_node: G::Node) -> Self {
        DijkstraIter {
            graph,
            distances: vec![(start_node.clone(), G::zero())].into_iter().collect(),
            visited: HashSet::new(),
            pq: PriorityQueue::from(vec![(start_node, ReverseOrder(G::zero()))]),
        }
    }

    pub fn update(&mut self, label: G::Node, alternative: G::Distance) {
        if self
            .distances
            .get(&label)
            .map(|distance| alternative < *distance)
            .unwrap_or(true)
        {
            self.distances.insert(label.clone(), alternative.clone());
            self.pq.push(label, ReverseOrder(alternative));
        }
    }
}

impl<'a, G> Iterator for DijkstraIter<'a, G>
where
    G: WGraph,
{
    type Item = (G::Node, G::Distance);

    fn next(&mut self) -> Option<Self::Item> {
        let (label, _) = self.pq.pop()?;
        self.visited.insert(label.clone());
        let distance = self.distances.get(&label).unwrap().clone();

        // consider portals accessible by walking
        for (neighbour, edge_len) in &self.graph.neighbours(&label) {
            if !self.visited.contains(neighbour) {
                self.update(neighbour.clone(), distance.clone().add(edge_len.clone()));
            }
        }

        Some((label, distance))
    }
}
