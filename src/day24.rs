use std::collections::{HashMap, HashSet};

const PUZZLE_INPUT: &str = include_str!("../inputs/24.txt");

macro_rules! bits {
    ($($num: expr),*) => {
        ($((1 << $num)+)* 0)
    }
}

// fuck it, I'll do this manually >:)
const OUTSIDE_MASK: [u32; 25] = [
    // row 0
    bits!(7, 11),
    bits!(7),
    bits!(7),
    bits!(7),
    bits!(7, 13),
    // row 1
    bits!(11),
    bits!(),
    bits!(),
    bits!(),
    bits!(13),
    // row 2
    bits!(11),
    bits!(),
    bits!(),
    bits!(),
    bits!(13),
    // row 3
    bits!(11),
    bits!(),
    bits!(),
    bits!(),
    bits!(13),
    // row 4
    bits!(17, 11),
    bits!(17),
    bits!(17),
    bits!(17),
    bits!(17, 13),
];

const MASK: [u32; 25] = [
    // row 0
    bits!(1, 5),
    bits!(0, 2, 6),
    bits!(1, 3, 7),
    bits!(2, 4, 8),
    bits!(3, 9),
    // row 1
    bits!(0, 6, 10),
    bits!(1, 5, 7, 11),
    bits!(2, 6, 8, 12),
    bits!(3, 7, 9, 13),
    bits!(4, 8, 14),
    // row 2
    bits!(5, 11, 15),
    bits!(6, 10, 12, 16),
    bits!(7, 11, 13, 17),
    bits!(8, 12, 14, 18),
    bits!(9, 13, 19),
    // row 3
    bits!(10, 16, 20),
    bits!(11, 15, 17, 21),
    bits!(12, 16, 18, 22),
    bits!(13, 17, 19, 23),
    bits!(14, 18, 24),
    // row 4
    bits!(15, 21),
    bits!(16, 20, 22),
    bits!(17, 21, 23),
    bits!(18, 22, 24),
    bits!(19, 23),
];

const INSIDE_MASK: [u32; 25] = [
    // row 0
    bits!(),
    bits!(),
    bits!(),
    bits!(),
    bits!(),
    // row 1
    bits!(),
    bits!(),
    bits!(0, 1, 2, 3, 4),
    bits!(),
    bits!(),
    // row 2
    bits!(),
    bits!(0, 5, 10, 15, 20),
    bits!(),
    bits!(4, 9, 14, 19, 24),
    bits!(),
    // row 3
    bits!(),
    bits!(),
    bits!(20, 21, 22, 23, 24),
    bits!(),
    bits!(),
    // row 4
    bits!(),
    bits!(),
    bits!(),
    bits!(),
    bits!(),
];

pub fn main() {
    part1();
    part2();
}

pub fn part1() {
    let mut board = biodiversity_from_string(PUZZLE_INPUT);
    let mut observed = HashSet::new();
    while !observed.contains(&board) {
        observed.insert(board);
        board = evolve(board);
    }
    println!("Part 1: {}", board);
}

fn biodiversity_from_string(input: &str) -> u32 {
    input
        .chars()
        .rev() // muy importante
        .flat_map(|c| match c {
            '.' => Some(0),
            '#' => Some(1),
            _ => None,
        })
        .fold(0, |acc, bit| (acc << 1) + bit)
}

// TRICK: the biodiversity score determines the grid
fn evolve(biodiversity: u32) -> u32 {
    (0..25)
        .rev() // muy importante
        .map(|i| {
            let num_alive_neighbours = (biodiversity & MASK[i]).count_ones();
            let alive = (biodiversity & bits!(i)).count_ones();
            match (alive, num_alive_neighbours) {
                (0, 1) | (0, 2) => 1,
                (0, _) => 0,
                (1, 1) => 1,
                (1, _) => 0,
                _ => unreachable!(),
            }
        })
        .fold(0, |acc, bit| (acc << 1) + bit)
}

pub fn part2() {
    let board = biodiversity_from_string(PUZZLE_INPUT);
    let mut levels: HashMap<isize, u32> = vec![(0, board)].into_iter().collect();

    for _ in 0..200 {
        levels = levels
            .keys()
            .chain(&[
                levels.keys().max().unwrap() + 1,
                levels.keys().min().unwrap() - 1,
            ])
            .map(|level| {
                let outside = levels.get(&(*level - 1)).cloned().unwrap_or(0);
                let here = levels.get(level).cloned().unwrap_or(0);
                let inside = levels.get(&(*level + 1)).cloned().unwrap_or(0);
                (*level, multi_level_evolve(outside, here, inside))
            })
            .collect();
    }
    println!(
        "Part 2: {}",
        levels
            .into_iter()
            .map(|(_, board)| board.count_ones())
            .sum::<u32>()
    );
}

fn kill_middle(biodiversity: u32) -> u32 {
    biodiversity - (biodiversity & bits!(12))
}

// TRICK: the biodiversity score determines the grid
fn multi_level_evolve(outside: u32, here: u32, inside: u32) -> u32 {
    (0..25)
        .rev() // muy importante
        .map(|i| {
            // ignore center piece
            if i == 12 {
                0
            } else {
                let alive = (here & bits!(i)).count_ones();
                let num_alive_neighbours = (kill_middle(outside) & OUTSIDE_MASK[i]).count_ones()
                    + (kill_middle(here) & MASK[i]).count_ones()
                    + (kill_middle(inside) & INSIDE_MASK[i]).count_ones();
                match (alive, num_alive_neighbours) {
                    (0, 1) | (0, 2) => 1,
                    (0, _) => 0,
                    (1, 1) => 1,
                    (1, _) => 0,
                    _ => unreachable!(),
                }
            }
        })
        .fold(0, |acc, bit| (acc << 1) + bit)
}
