use crate::intcode::*;
use network::*;
use std::collections::*;

const PUZZLE_INPUT: &str = include_str!("../inputs/23.txt");

pub fn main() {
    part1();
    part2();
}

fn part1() {
    let program = IntCodeMachine::from_string(PUZZLE_INPUT);
    let mut network = Network::new(50, &program);
    // NOTE: potential infinite loop
    let msg = (0..)
        .flat_map(|_| network.round_robin())
        .find(|m| m.addr == 255);
    println!("Part 1: {:?}", msg.unwrap().y);
}

fn part2() {
    let program = IntCodeMachine::from_string(PUZZLE_INPUT);
    let mut network = Network::new(50, &program);
    let mut seen_y_values = HashSet::new();
    // NOTE: potential infinite loop
    for (nat, round) in (0..).map(|_| (network.nat.clone(), network.round_robin())) {
        if round.is_empty() {
            // check for idleness
            if let Some(Message { y, .. }) = nat {
                if seen_y_values.contains(&y) {
                    println!("Part 2: {:?}", y);
                    break;
                } else {
                    seen_y_values.insert(y);
                }
            }
        }
    }
}

pub mod network {
    use crate::intcode::*;
    use std::collections::*;

    #[derive(Clone, Copy, Debug)]
    pub struct Message {
        pub addr: usize,
        pub x: i64,
        pub y: i64,
    }

    pub struct Network {
        nodes: Vec<IntCodeMachine>,
        qs: Vec<VecDeque<Message>>,
        pub nat: Option<Message>,
    }

    impl Network {
        pub fn new(num: usize, program: &IntCodeMachine) -> Self {
            Network {
                nodes: (0..num)
                    .map(|addr| program.clone().with_input(vec![addr as i64]))
                    .collect(),
                qs: (0..num).map(|_| VecDeque::new()).collect(),
                nat: None,
            }
        }

        pub fn round_robin(&mut self) -> Vec<Message> {
            let mut log = vec![];
            let arange = self.address_range();
            for addr in arange.clone() {
                let node = &mut self.nodes[addr];
                let mut queue: Vec<_> = std::mem::replace(&mut self.qs[addr], VecDeque::new())
                    .into_iter()
                    .flat_map(|message| vec![message.x, message.y])
                    .collect();
                if queue.is_empty() {
                    queue.push(-1);
                }
                node.inputs.push_vec(queue);
                while let Some(message) = listen(node) {
                    if arange.contains(&message.addr) {
                        self.qs[message.addr].push_back(message);
                        log.push(message);
                    } else if message.addr == 255 {
                        self.nat = Some(message);
                        log.push(message);
                    } else {
                        panic!("unknown address {}", message.addr);
                    }
                }
            }

            // check for idleness
            if log.is_empty() {
                if let Some(mut message) = self.nat {
                    message.addr = 0;
                    self.qs[0].push_back(message);
                } else {
                    panic!("NAT: unable to resume");
                }
            }
            log
        }

        pub fn address_range(&self) -> std::ops::Range<usize> {
            0..self.nodes.len()
        }
    }

    fn listen(node: &mut IntCodeMachine) -> Option<Message> {
        Some(Message {
            addr: node.next_output()? as usize,
            x: node.next_output()?,
            y: node.next_output()?,
        })
    }
}
