use rayon::prelude::*;
use std::collections::{HashMap, HashSet};

pub fn main() {
    part1();
    part2();
}

fn get_input() -> HashMap<String, String> {
    include_str!("../inputs/06.txt")
        .lines()
        .map(|line| line.split(')').take(2).collect::<Vec<_>>())
        .map(|pair| (pair[1].to_owned(), pair[0].to_owned()))
        .collect::<HashMap<_, _>>()
}

fn part1() {
    let orbits = get_input();

    // we calculate for each object its contribution to the hash
    // using no memoization (more expensive)
    println!(
        "Part 1: {}",
        orbits
            .keys()
            .collect::<Vec<_>>()
            .into_par_iter()
            .map(|o| parents(&o, &orbits).len())
            .sum::<usize>()
    );
}

fn parents(o: &str, orbits: &HashMap<String, String>) -> HashSet<String> {
    (1..)
        .scan(o.to_owned(), |o, _| {
            orbits.get(o).map(|oo| {
                *o = oo.to_owned();
                o.to_owned()
            })
        })
        .collect()
}

fn part2() {
    let orbits = get_input();

    // symmetric difference idea due to @mstk
    // https://github.com/mstksg/advent-of-code-2019/blob/master/reflections/day06.md
    println!(
        "Part 2: {:?}",
        parents("YOU", &orbits)
            .symmetric_difference(&parents("SAN", &orbits))
            .count()
    )
}
