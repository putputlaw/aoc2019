use crate::intcode::*;

const PUZZLE_INPUT: &str = include_str!("../inputs/21.txt");

pub fn main() {
    let script1: [&str; 5] = [
        "NOT C J", // if no hole at C, jump
        "AND D J", // if hole at D, don't jump
        "NOT A T", "OR T J", // if hole at A, do jump
        "WALK\n",
    ];

    let script2: [&str; 9] = [
        "OR A J", "AND B J", "AND C J", "NOT J J", // If no holes in A,B,C, don't jump
        "OR E T", "OR H T", "AND T J", // If holes at E and H, do jump
        "AND D J", // If hole at D, don't jump
        "RUN\n",
    ];

    println!("Part 1: {}", run_script(&script1.join("\n"), true));
    println!("Part 2: {}", run_script(&script2.join("\n"), false));
}

fn run_script(script: &str, silent: bool) -> i64 {
    let mut program = IntCodeMachine::from_string(PUZZLE_INPUT);
    let program_input: Vec<i64> = script.chars().map(|ch| (ch as u8) as i64).collect();
    program.inputs.push_vec(program_input);

    let mut output = vec![-1];
    while !program.is_halted() && !output.is_empty() {
        output = program.run();
        match output.iter().find(|&&i| i > 255) {
            Some(damage) => {
                return *damage;
            }
            None if !silent => {
                let output_string: String = output.iter().map(|i| (*i as u8) as char).collect();
                println!("{}", output_string);
            }
            _ => {}
        }
    }
    -1
}
