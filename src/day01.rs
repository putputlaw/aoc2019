pub fn main() {
    part1();
    part2();
}

fn get_input() -> Vec<u32> {
    include_str!("../inputs/01.txt")
        .lines()
        .flat_map(|line| line.parse())
        .collect()
}

fn part1() {
    let fuel = |n: u32| n / 3 - 2;
    println!("Part 1: {}", get_input().into_iter().map(fuel).sum::<u32>());
}

fn part2() {
    fn rec_fuel(n: u32) -> u32 {
        if n / 3 > 1 {
            let m = n / 3 - 2;
            m + rec_fuel(m)
        } else {
            0
        }
    }
    println!(
        "Part 2: {}",
        get_input().into_iter().map(rec_fuel).sum::<u32>()
    );
}
