use dungeon::*;

const PUZZLE_INPUT: &str = include_str!("../inputs/18.txt");

pub fn main() {
    let dungeon = Dungeon::from_string(PUZZLE_INPUT);
    println!(
        "Part 1: {:?}",
        dungeon.find_all_keys().expect("could not find all keys")
    );
    println!("Part 2: {:?}", part2());
}

pub fn part2() {}

pub mod dungeon {
    use crate::geometry::point::Point;
    use crate::geometry::rectangle::Rectangle;
    use crate::geometry::tiles::Tiles;
    use crate::intset64::IntSet;
    use crate::shortest_paths::reverse_order::ReverseOrder;
    use priority_queue::PriorityQueue;
    use std::collections::*;

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum Tile {
        Key(u8),
        Door(u8),
        Start(usize),
    }

    /// None = Space
    type TileOrSpace = Option<Tile>;

    #[derive(Clone, Debug)]
    pub struct KeyFamily(Vec<IntSet>);

    /// A `KeyFamily` is a set of `IntSet`s which is downward closed,
    /// i.e. if it contains `x : IntSet` and `x.is_superset_of(y)` holds, then
    /// it contains also `y`.
    /// In order to save space and time, only the maximum elements are stored.
    impl KeyFamily {
        pub fn empty() -> Self {
            KeyFamily(vec![])
        }

        pub fn singleton(set: IntSet) -> Self {
            KeyFamily(vec![set])
        }

        pub fn insert(&mut self, set: IntSet) {
            if !self.contains(set) {
                self.0 = self
                    .0
                    .iter()
                    .filter(|s| !s.is_subset_of(set))
                    .chain(&[set])
                    .cloned()
                    .collect();
            }
        }

        pub fn supersets_of(&self, set: IntSet) -> KeyFamily {
            KeyFamily(
                self.0
                    .iter()
                    .filter(|s| s.is_superset_of(set))
                    .cloned()
                    .collect(),
            )
        }

        pub fn is_empty(&self) -> bool {
            self.0.is_empty()
        }

        pub fn contains(&self, set: IntSet) -> bool {
            !self.supersets_of(set).is_empty()
        }
    }

    #[derive(Debug, Clone)]
    pub struct Dungeon {
        // None = Space
        pub tiles: Tiles<TileOrSpace>,
        pub bounding_rect: Rectangle,
        pub keys: IntSet,
        pub positions: HashMap<Tile, Point>,
        pub num_bots: usize,
        pub reduced_graph: HashMap<Tile, HashMap<Tile, Distance>>,
    }

    impl Dungeon {
        pub fn from_string(input: &str) -> Dungeon {
            let mut num_bots = 0;
            let mut keys = IntSet::empty();
            let mut positions = HashMap::new();
            let mut bounding_rect = Rectangle::empty();

            let tiles = Tiles::from_string(input, |pt, ch| {
                bounding_rect = bounding_rect.grow(pt);
                match ch {
                    '.' => Some(None),
                    '@' => {
                        let bot_id = num_bots;
                        num_bots += 1;
                        positions.insert(Tile::Start(bot_id), pt);
                        Some(Some(Tile::Start(bot_id)))
                    }
                    // NOTE: memorable bug: I used `'a'..'z'`, resp. `'A'..'Z'`,
                    // before, which failed me on the large mazes... Amazin'...
                    _ if ('a'..='z').contains(&ch) => {
                        let id = ch as u8 - b'a';
                        keys.insert(id as usize);
                        positions.insert(Tile::Key(id), pt);
                        Some(Some(Tile::Key(id)))
                    }
                    _ if ('A'..='Z').contains(&ch) => {
                        let id = ch as u8 - b'A';
                        positions.insert(Tile::Door(id), pt);
                        Some(Some(Tile::Door(id)))
                    }
                    _ => None,
                }
            });

            let mut reduced_graph = HashMap::new();

            for (&tile, &pt) in &positions {
                let neighbours = reduced_graph.entry(tile).or_insert_with(HashMap::new);
                for (_, neighbour, dist) in DirectNeighboursIter::from_tiles(&tiles, pt) {
                    if let Some(neighbour) = neighbour {
                        if dist > 0 {
                            neighbours.insert(neighbour, dist);
                        }
                    }
                }
            }

            Dungeon {
                tiles,
                bounding_rect,
                keys,
                num_bots,
                positions,
                reduced_graph,
            }
        }

        pub fn format_map(&self) -> String {
            let mut rows = vec![];
            for y in self.bounding_rect.y_range().rev() {
                rows.push(
                    self.bounding_rect
                        .x_range()
                        .map(|x| match self.tiles.get((x, y)) {
                            None => "　".to_string(),
                            Some(Some(Tile::Key(_))) => "き".to_string(),
                            Some(Some(Tile::Door(_))) => "門".to_string(),
                            Some(Some(Tile::Start(_))) => "始".to_string(),
                            Some(None) => "ロ".to_string(),
                        })
                        .collect::<String>(),
                )
            }
            rows.join("\n")
        }

        pub fn find_all_keys(&self) -> Option<Distance> {
            for node in DijkstraIter::new(
                &self,
                (0..self.num_bots).map(|id| Tile::Start(id)).collect(),
            ) {
                if node.keyring.is_superset_of(self.keys) {
                    return Some(node.distance);
                }
            }
            None
        }
    }

    type Distance = usize;

    pub struct DirectNeighboursIter<'a> {
        start: Point,
        tiles: &'a Tiles<Option<Tile>>,
        queue: VecDeque<(Point, TileOrSpace, Distance)>,
        visited: HashSet<Point>,
    }

    impl<'a> DirectNeighboursIter<'a> {
        pub fn from_tiles(tiles: &'a Tiles<TileOrSpace>, start: Point) -> Self {
            DirectNeighboursIter {
                start,
                tiles,
                queue: tiles
                    .get(start)
                    .map(|tile| VecDeque::from(vec![(start, tile, 0)]))
                    .unwrap_or_default(),
                visited: HashSet::new(),
            }
        }

        pub fn neighbours(tiles: &Tiles<TileOrSpace>, pt: Point) -> Vec<(Point, TileOrSpace)> {
            [
                (pt.0 - 1, pt.1),
                (pt.0 + 1, pt.1),
                (pt.0, pt.1 - 1),
                (pt.0, pt.1 + 1),
            ]
            .iter()
            .flat_map(|&pt| tiles.get(pt).map(|tile| (pt, tile)))
            .collect()
        }
    }

    impl<'a> Iterator for DirectNeighboursIter<'a> {
        type Item = (Point, TileOrSpace, usize);

        fn next(&mut self) -> Option<Self::Item> {
            let (pt, tile, dist) = self.queue.pop_front()?;
            self.visited.insert(pt);
            if pt == self.start || tile.is_none() {
                for (neighbour, tile) in DirectNeighboursIter::neighbours(&self.tiles, pt) {
                    if !self.visited.contains(&neighbour) {
                        self.queue.push_back((neighbour, tile, dist + 1));
                    }
                }
            }
            Some((pt, tile, dist))
        }
    }

    #[derive(Debug, Clone, PartialEq, Eq, Hash)]
    pub struct Node {
        bots: Vec<Tile>,
        keyring: IntSet,
        distance: usize,
    }

    impl Node {
        pub fn new(bots: Vec<Tile>, keyring: IntSet, distance: usize) -> Self {
            Node {
                bots,
                keyring,
                distance,
            }
        }
    }

    #[derive(Debug, Clone)]
    pub struct DijkstraIter<'a> {
        dungeon: &'a Dungeon,
        visited: HashMap<Tile, KeyFamily>,
        pq: PriorityQueue<Node, ReverseOrder<Distance>>,
    }

    impl<'a> DijkstraIter<'a> {
        fn new(dungeon: &'a Dungeon, bots: Vec<Tile>) -> Self {
            DijkstraIter {
                dungeon,
                visited: HashMap::new(),
                pq: PriorityQueue::from(vec![(
                    Node::new(bots, IntSet::empty(), 0),
                    ReverseOrder(0),
                )]),
            }
        }
    }

    impl<'a> Iterator for DijkstraIter<'a> {
        type Item = Node;
        fn next(&mut self) -> Option<Self::Item> {
            let node = self.pq.pop()?.0;
            for (bot_id, &tile) in node.bots.iter().enumerate() {
                self.visited
                    .entry(tile)
                    .and_modify(|kf| kf.insert(node.keyring))
                    .or_insert_with(|| KeyFamily::singleton(node.keyring));
                for (neighbour, edge_len) in
                    self.dungeon.reduced_graph.get(&tile).into_iter().flatten()
                {
                    let neighbour_keyfam = self
                        .visited
                        .entry(*neighbour)
                        .or_insert_with(KeyFamily::empty);
                    if !neighbour_keyfam.contains(node.keyring) {
                        let new_distance = node.distance + edge_len;
                        let priority = ReverseOrder(new_distance);
                        let mut bots = node.bots.clone();
                        bots[bot_id] = *neighbour;
                        match neighbour {
                            Tile::Start(_) => {
                                self.pq
                                    .push(Node::new(bots, node.keyring, new_distance), priority);
                            }
                            &Tile::Door(id) => {
                                if node.keyring.contains(id as usize) {
                                    self.pq.push(
                                        Node::new(bots, node.keyring, new_distance),
                                        priority,
                                    );
                                }
                            }
                            &Tile::Key(id) => {
                                let mut keyring = node.keyring;
                                keyring.insert(id as usize);
                                self.pq
                                    .push(Node::new(bots, keyring, new_distance), priority);
                            }
                        }
                    }
                }
            }
            Some(node)
        }
    }
}
