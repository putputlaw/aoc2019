use crate::intcode::*;
use droid::*;
use std::cell::RefCell;
use std::rc::Rc;

const PUZZLE_INPUT: &str = include_str!("../inputs/15.txt");

pub fn main() {
    let (droid, dist) = find_oxygen_tank(parse_input(PUZZLE_INPUT)).unwrap();
    println!("Part 1: {}", dist);

    let fill_time = oxygen_fill_time(droid);
    println!("Part 2: {}", fill_time);
}

fn parse_input(input: &str) -> Droid {
    Droid::new(IntCodeMachine::from_string(input))
}

/// returns droid right after it found oxygen tank
fn find_oxygen_tank(droid0: Droid) -> Option<(Droid, usize)> {
    breadth_first_search(droid0, 0, |droid, status, dist| match status {
        Status::Okay => BFS::Queue(dist + 1),
        Status::Found => BFS::Return((droid.clone(), dist + 1)),
        Status::Wall => BFS::Ignore,
    })
}

fn oxygen_fill_time(droid0: Droid) -> usize {
    let max_distance = Rc::new(RefCell::new(0));
    breadth_first_search::<_, _, ()>(droid0, 0, |_, status, dist| match status {
        Status::Wall => BFS::Ignore,
        _ => {
            max_distance.replace_with(|&mut m| m.max(dist + 1));
            BFS::Queue(dist + 1)
        }
    });
    max_distance.replace(0)
}

pub mod droid {
    use crate::intcode::*;
    use std::collections::{HashSet, VecDeque};

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub struct Droid(IntCodeMachine);

    /// cardinal direction
    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum CardDir {
        North,
        South,
        East,
        West,
    }

    impl CardDir {
        pub fn apply(self, (x, y): (i64, i64)) -> (i64, i64) {
            use CardDir::*;
            match self {
                North => (x, y + 1),
                South => (x, y - 1),
                West => (x - 1, y),
                East => (x + 1, y),
            }
        }
    }

    impl Into<i64> for CardDir {
        fn into(self) -> i64 {
            use CardDir::*;
            match self {
                North => 1,
                South => 2,
                West => 3,
                East => 4,
            }
        }
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum Status {
        Wall,
        Okay,
        Found,
    }

    impl Droid {
        pub fn new(program: IntCodeMachine) -> Droid {
            Droid(program)
        }

        pub fn go(&mut self, dir: CardDir) -> Option<Status> {
            self.0.inputs.fallback = Some(dir.into());
            match self.0.next_output()? {
                0 => Some(Status::Wall),
                1 => Some(Status::Okay),
                2 => Some(Status::Found),
                _ => panic!("program error"),
            }
        }
    }

    pub enum BFS<T, R> {
        Queue(T),
        Return(R),
        Ignore,
    }

    // BFS, where we send nondeterministic bots around the area
    pub fn breadth_first_search<T, F, R>(droid0: Droid, data0: T, action: F) -> Option<R>
    where
        T: Clone,
        F: Fn(&Droid, Status, T) -> BFS<T, R>,
    {
        let mut queue = VecDeque::from(vec![(droid0, (0, 0), data0)]);
        let mut visited: HashSet<(i64, i64)> = vec![(0, 0)].into_iter().collect();
        loop {
            let (droid, pos, data) = queue.pop_front()?;
            use CardDir::*;
            for &dir in &[North, South, East, West] {
                let mut probe = droid.clone();
                if visited.get(&dir.apply(pos)).is_none() {
                    visited.insert(dir.apply(pos));
                    if let Some(status) = probe.go(dir) {
                        match action(&probe, status, data.clone()) {
                            BFS::Queue(new_data) => {
                                queue.push_back((probe, dir.apply(pos), new_data))
                            }
                            BFS::Ignore => {}
                            BFS::Return(result) => return Some(result),
                        }
                    }
                }
            }
        }
    }
}
