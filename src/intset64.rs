#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub struct IntSet(u64);

impl Default for IntSet {
    fn default() -> Self {
        IntSet(0)
    }
}

impl IntSet {
    pub fn empty() -> Self {
        IntSet::default()
    }

    pub fn insert(&mut self, n: usize) {
        assert!(n < 64);
        self.0 |= 1 << n
    }

    pub fn contains(self, n: usize) -> bool {
        assert!(n < 64);
        self.0 & (1 << n) == (1 << n)
    }

    pub fn remove(&mut self, n: usize) {
        assert!(n < 64);
        self.0 -= self.0 & (1 << n)
    }

    pub fn is_superset_of(self, other: Self) -> bool {
        other.0 & self.0 == other.0
    }

    pub fn is_subset_of(self, other: Self) -> bool {
        other.0 & self.0 == self.0
    }

    pub fn union(self, other: Self) -> Self {
        IntSet(self.0 | other.0)
    }

    pub fn intersection(self, other: Self) -> Self {
        IntSet(self.0 & other.0)
    }

    pub fn symmetric_difference(self, other: Self) -> Self {
        use std::ops::BitXor;
        IntSet(self.0.bitxor(other.0))
    }

    pub fn len(self) -> u32 {
        self.0.count_ones()
    }

    pub fn is_empty(self) -> bool {
        self.0 == 0
    }

    pub fn iter<'a>(self) -> impl Iterator<Item=usize> + 'a {
        (0..64).filter(move |i| self.contains(*i))
    }
}

impl From<u64> for IntSet {
    fn from(bits: u64) -> Self {
        IntSet(bits)
    }
}

impl From<Vec<usize>> for IntSet {
    fn from(ints: Vec<usize>) -> Self {
        let mut set = IntSet::empty();
        for i in ints.into_iter() {
            set.insert(i);
        }
        set
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn intset_test() {
        let mut s = IntSet::empty();
        s.insert(1);
        assert_eq!(s, IntSet(2));
        s.insert(3);
        assert_eq!(s, IntSet(10));
        assert!(!s.contains(2));
        assert!(s.contains(1));
        assert!(s.contains(3));
        s.insert(25);
        assert_eq!(s, IntSet(33554432 + 10));
        assert_eq!(s.iter().collect::<Vec<_>>(), vec![1,3,25]);
    }

    #[test]
    fn from_vec_test() {
        let mut s = IntSet::empty();
        s.insert(1);
        s.insert(2);
        s.insert(32);
        let t = IntSet::from(vec![1usize,2,32]);
        assert_eq!(s, t);
    }

    #[test]
    fn xor_test() {
        let s = IntSet::from(vec![1,12,44,53]);
        let t = IntSet::from(vec![1,18,44,50]);
        assert_eq!(s.symmetric_difference(t), IntSet::from(vec![12,18,50,53]))
    }
}
