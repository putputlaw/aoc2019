# aoc2019

Advent of Code 2019 (Rust)

Status
- day 25
- days 19..=24 **
- day 18 *
- days 1..=17 **


# Check out these other repositories

- [Jasper van der Jeugt](https://github.com/jaspervdj/advent-of-code/tree/master/2019) ain't no quitter! Solutions delivered every day.
- You can learn a lot from [Armin Ronacher (@mitsuhiko)](https://github.com/mitsuhiko/aoc19/), who writes very clean rust solutions.
- [Justin Le (@mstk)](https://github.com/mstksg/advent-of-code-2019/) showcases the elegance of Haskell.
- He loves them spread sheets: [u/pngipngi](https://github.com/pengi/advent_of_code)
- To [Chris Penner](https://chrispenner.ca/posts/advent-of-optics-01), author of "Optics by Example", every problem looks like a nail.
- [Bartosz Milewski](https://github.com/BartoszMilewski/AoC2019) likes it category theory flavoured.
