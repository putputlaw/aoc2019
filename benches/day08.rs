use aoc2019::run;
use criterion::{black_box, criterion_group, criterion_main, Criterion};

fn criterion_benchmark(c: &mut Criterion) {
    let day = 08;
    c.bench_function(&format!("day {}", day), |b| b.iter(|| run(black_box(day))));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
